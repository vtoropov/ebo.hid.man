/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-May-2020 at 6:31:48p, UTC+7, Novosibirsk, Thursday;
	This is Ebo Xor Java Eclipse build tool client app entry point implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo VAC driver client project on 22-Jun-2020 at 7:28:45a, UTC+7, Monday;
	Adopted to Ebo Pack Hide Man project on 6-Jul-2020 at 07:14:54a, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

#include "shared.gen.sys.com.h"

using namespace shared::sys_core;

#include "ebo.hid.man.dlg.h"

CAppModule _Module;

#define _MODAL
/////////////////////////////////////////////////////////////////////////////

INT RunModal(VOID) {
	INT nRet = 0;
	ebo::hide::gui::CHideManDlg dlg_man;

	HRESULT hr_ = dlg_man.DoModal();
	if (SUCCEEDED(hr_)) {
	}

	return nRet;
}

INT RunModeless(VOID) {
	INT result_ = 0; return result_;
}

INT WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpstrCmdLine, INT nCmdShow)
{
	hInstance; hPrevInstance; lpstrCmdLine; nCmdShow;

	CCoIniter com(false);
	CCoSecurityProvider sec_;

	HRESULT hr_ = sec_.InitDefault();
	ATLVERIFY(SUCCEEDED(hr_));
	/*
		Tech_dog commented on 09-Feb-2010 at 12:47:50pm:
		________________________________________________
		we need to assign lib id to link ATL DLL statically,
		otherwise we get annoying fkn message "Did you forget to pass the LIBID to CComModule::Init?"
	*/
	_Module.m_libid = LIBID_ATLLib;
	INT nResult = 0;
	// this resolves ATL window thunking problem when Microsoft Layer for Unicode (MSLU) is used
	::DefWindowProc(NULL, 0, 0, 0L);

	AtlInitCommonControls(ICC_BAR_CLASSES);	// add flags to support other controls

	hr_ = _Module.Init(NULL, hInstance);
	if(!SUCCEEDED(hr_))
	{
		ATLASSERT(FALSE);
		return -1;
	}

	ex_ui::draw::CGdiPlusLib_Guard  guard_;
#if defined(_MODAL)
	nResult = ::RunModal();
#else
	nResult = ::RunModeless();
#endif
	_Module.Term();

	return nResult;
}