//
// Created by Tech_dog (ebontrop@gmail.com) on 12-May-2020 at 6:24:55a, UTC+7, Novosibirsk, Tuesday;
// This is FakeGPS driver configuration dialog identifier declaration file.
// -----------------------------------------------------------------------------
// Adopted to Ebo VAC driver client project on 22-Jun-2020 at 8:02:11a, UTC+7, Monday;
// Adopted to Ebo Pack Hide Man project on 6-Jul-2020 at 06:06:05a, UTC+7, Novosibirsk, Monday;
//

#pragma region __cfg_dlg

#define IDD_EBO_HID_MAN_DLG        3301
#define IDR_EBO_HID_MAN_DLG_ICO    3303

#define IDC_EBO_HID_MAN_SAVE       3305
#define IDC_EBO_HID_MAN_BAN        3307
#define IDR_EBO_HID_MAN_BAN        IDC_EBO_HID_MAN_BAN
#define IDC_EBO_HID_MAN_TAB        3309
#define IDC_EBO_HID_MAN_SEP        3311

#pragma region __cfg_1st_page

#define IDD_EBO_HID_MAN_1ST_PAG    3401

#define IDC_EBO_HID_MAN_SVC_AVA    3403
#define IDR_EBO_HID_MAN_SVC_AVA    IDC_EBO_HID_MAN_SVC_AVA
#define IDC_EBO_HID_MAN_SVC_CAP    3405
#define IDC_EBO_HID_MAN_SVC_SEP    3407

#define IDC_EBO_HID_MAN_SVC_SETUP  3409
#define IDC_EBO_HID_MAN_SVC_INSTL  3411

#define IDC_EBO_HID_MAN_SVC_STATE  3413
#define IDC_EBO_HID_MAN_SVC_CTRL   3415
#define IDC_EBO_HID_MAN_SVC_CHECK  3417

#define IDC_EBO_HID_MAN_LOC_AVA    3419
#define IDC_EBO_HID_MAN_LOC_CAP    3421
#define IDC_EBO_HID_MAN_LOC_SEP    3423

#define IDR_EBO_HID_MAN_LOC_AVA    IDC_EBO_HID_MAN_LOC_AVA

#define IDC_EBO_HID_MAN_LOC_PATH   3425
#define IDC_EBO_HID_MAN_LOC_LOOK   3427

#define IDC_EBO_HID_MAN_INF_IMG    3429
#define IDC_EBO_HID_MAN_INF_TXT    3431

#define IDR_EBO_HID_MAN_INF_IMG    IDC_EBO_HID_MAN_INF_IMG
#define IDS_EBO_HID_SVC_INF_TXT    3435

#define IDS_EBO_HID_SVC_ERR_TXT    3437
#define IDR_EBO_HID_MAN_ERR_IMG    3439

#pragma endregion

#pragma region __cfg_2nd_page

#define IDD_EBO_HID_MAN_2ND_PAG    3501

#define IDC_EBO_HID_MAN_EXC_AVA    3503
#define IDR_EBO_HID_MAN_EXC_AVA    IDC_EBO_HID_MAN_EXC_AVA
#define IDC_EBO_HID_MAN_EXC_CAP    3505
#define IDC_EBO_HID_MAN_EXC_SEP    3507

#pragma endregion

#pragma endregion


