#ifndef _FGDESKTOPSVCCTRLS_H_CE3271EC_639C_4f55_A50A_4C6B37E6586B_INCLUDED
#define _FGDESKTOPSVCCTRLS_H_CE3271EC_639C_4f55_A50A_4C6B37E6586B_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) 11-Jan-2018 at 8:52:24p, UTC+7, Phuket, Rawai, Thursday;
	This is File Guardian background service desktop UI control interface declaration file.
	(project: https://fileguardian.com)
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Hide Man project on 15-Jul-2020 at 12:08:18p, UTC+7, Novosibirsk, Wednesday;
*/
#include "shared.gen.sys.err.h"
namespace ebo { namespace hide { namespace gui { namespace _impl {

	using shared::sys_core::CError;

	class CService_Ctrls {
	private:
		CWindow&  m_page_ref;
		CError    m_error;

	public:
		 CService_Ctrls (CWindow& page_ref);
		~CService_Ctrls (void);

	public:
		TErrorRef Error     (void) const;
		HRESULT   Initialize(void);
		HRESULT   InitializeForWait(void);
		BOOL      OnCommand (const WORD wCtrlId, const WORD wNotifyCode);
	};

	class CService_Auto {

	private:
		UINT_PTR  m_timer;
		CWindow   m_owner;

	public:
		 CService_Auto (void);
		~CService_Auto (void);

	public:
		VOID      Create (const DWORD _period_ms = 2000); // by default, a check action occurs once per 2 seconds;
		VOID      Destroy(void);
		VOID      ManageByCtrl (const UINT ctrlId);
		VOID      ManageByState(const bool _auto_check);
		HWND      Owner(void)const;
		HRESULT   Owner(const CWindow& _owner);

	public:
		CService_Auto& GetObject (void); // returns static instance of the timer;
	};

}}}}

#endif/*_FGDESKTOPSVCCTRLS_H_CE3271EC_639C_4f55_A50A_4C6B37E6586B_INCLUDED*/