#ifndef _EBOHIDEMANTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
#define _EBOHIDEMANTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-May-2020 at 0:52:22a, UTC+7, Novosibirsk, Wednesday;
	This is FakeGPS driver configuration dialog tab set interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo VAC driver client project on 23-Jun-2020 at 2:55:19a, UTC+7, Tuesday;
	Adopted to Ebo Pack Hide Man project on 6-Jul-2020 at 06:50:33a, UTC+7, Novosibirsk, Monday;
*/
#include "shared.gui.page.base.h"

#include "ebo.hid.man.pag.1st.h"
//#include "ebo.vac.drv.pag.2nd.h"
//#include "ebo.vac.drv.pag.3rd.h"

namespace ebo { namespace hide { namespace gui {

	using shared::gui::ITabSetCallback;

	class CHideTabSet
	{
	private:
		WTL::CTabCtrl   m_cTabCtrl;

	private: //tab page(s)
		CPageMan_1st    m_service ;
//		CPageDrv_2nd    m_exchange;
//		CPageDrv_3rd    m_setup   ;

	public:
		 CHideTabSet (ITabSetCallback&);
		~CHideTabSet (void);

	public:
		HRESULT       Create  (const HWND hParent, const RECT& rcArea);
		HRESULT       Destroy (void)        ;

	public:
		HRESULT       UpdateData  (void);
		VOID          UpdateLayout(void);
	};
}}}

#endif/*_EBOHIDEMANTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED*/