#ifndef _EBOHIDMANPAG1STDEF_H_09CCEA40_6B37_44B9_B98F_F0E353D3E455_INCLUDED
#define _EBOHIDMANPAG1STDEF_H_09CCEA40_6B37_44B9_B98F_F0E353D3E455_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Jul-2020 at 3:04:58p, UTC+7, Novosibirsk, Saturday;
	This is Ebo Hide Man monitor service configuration dialog page control identifier declaration file.
*/
#include "ebo.hid.man.dlg.res.h"
namespace ebo { namespace hide { namespace gui { namespace _impl {

	class CPageMan_1st_Ctl {
	public:
		enum _ctl : WORD {
			e_svc_ava = IDC_EBO_HID_MAN_SVC_AVA  ,    // connect section avatar  ;
			e_svc_cap = IDC_EBO_HID_MAN_SVC_CAP  ,    // connect page caption    ;
			e_edt_ins = IDC_EBO_HID_MAN_SVC_SETUP,    // service install status  ; editbox|readonly;
			e_but_ins = IDC_EBO_HID_MAN_SVC_INSTL,    // service install button  ;
			e_edt_sta = IDC_EBO_HID_MAN_SVC_STATE,    // service current state   ; editbox|readonly;
			e_but_sta = IDC_EBO_HID_MAN_SVC_CTRL ,    // service state control   ;
			e_chk_sta = IDC_EBO_HID_MAN_SVC_CHECK,    // service state auto check;

			e_loc_ava = IDC_EBO_HID_MAN_LOC_AVA  ,    // service location avatar ;
			e_loc_cap = IDC_EBO_HID_MAN_LOC_CAP  ,    // service location caption;

			e_edt_loc = IDC_EBO_HID_MAN_LOC_PATH ,    // service file location   ; editbox|readonly;
			e_but_loc = IDC_EBO_HID_MAN_LOC_LOOK ,    // service file browse button;

			e_inf_img = IDC_EBO_HID_MAN_INF_IMG  ,    // service install info image ctrl ;
			e_inf_txt = IDC_EBO_HID_MAN_INF_TXT  ,    // service install info text label ;
		};
	};
	typedef CPageMan_1st_Ctl This_Ctl;

	class CPageMan_1st_Res {
	public:
		enum _res : WORD {
			e_svc_ava = IDR_EBO_HID_MAN_SVC_AVA  ,
			e_loc_ava = IDR_EBO_HID_MAN_LOC_AVA  ,
			e_inf_img = IDR_EBO_HID_MAN_INF_IMG  ,
			e_inf_txt = IDS_EBO_HID_SVC_INF_TXT  ,
			e_err_img = IDR_EBO_HID_MAN_ERR_IMG  ,
			e_err_txt = IDS_EBO_HID_SVC_ERR_TXT  ,
		};
	};
	typedef CPageMan_1st_Res This_Res;

}}}}

#endif/*_EBOHIDMANPAG1STDEF_H_09CCEA40_6B37_44B9_B98F_F0E353D3E455_INCLUDED*/