/*
	Created by Tech_dog (ebontrop@gmail.com) 11-Jan-2018 at 9:02:43p, UTC+7, Phuket, Rawai, Thursday;
	This is File Guardian background service desktop UI control interface implementation file.
	(project: https://fileguardian.com)
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Hide Man project on 15-Jul-2020 at 12:15:54p, UTC+7, Novosibirsk, Wednesday;
*/
#include "StdAfx.h"
#include "ebo.hid.man.pag.1st.ctl_flow.h"
#include "ebo.hid.man.pag.1st.def.h"

using namespace ebo::hide::gui;

#include "ebo.hid.svc.ctl.h"
#include "ebo.hid.svc.cfg.h"

using namespace ebo::hide::cfg;
using namespace ebo::hide::service;

#include "sys.svc.create.h"
#include "sys.svc.setup.h"

using namespace shared::service;

#include "shared.gui.page.layout.h"

using namespace shared::gui::layout;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace hide { namespace gui { namespace _impl
{
	CControlManager& CtrlMan_Object(void)
	{
		static CControlManager ctl_man;
		return ctl_man;
	}

	CService_Auto& CSvcTimer_Object(void)
	{
		static CService_Auto timer_;
		return timer_;
	}
}}}}
using namespace ebo::hide::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CService_Ctrls:: CService_Ctrls(CWindow& page_ref) : m_page_ref(page_ref) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CService_Ctrls::~CService_Ctrls(void) { }

/////////////////////////////////////////////////////////////////////////////

TErrorRef CService_Ctrls::Error (void) const { return m_error; }

HRESULT   CService_Ctrls::Initialize(void) {
	m_error << __MODULE__ << S_OK;

	const CService& svc_ref = ((CControlManager&)CtrlMan_Object()).GetObject();

	bool bServiceInstalled = (S_OK == CtrlMan_Object().Installed());
	if ( bServiceInstalled == false )
		m_error = CtrlMan_Object().Error();

	CWindow button = Lay_(m_page_ref) << This_Ctl::e_but_ins;
	if (button)
		button.SetWindowText(bServiceInstalled ? _T("Uninstall") : _T("Install"));

	::WTL::CEdit edt_;
	edt_ = Lay_(m_page_ref) << This_Ctl::e_edt_ins;
	if (edt_) {
		edt_.SetWindowText(bServiceInstalled ? _T("Installed") : _T("Uninstalled"));
		edt_.EnableWindow (bServiceInstalled);
	}

	edt_ = Lay_(m_page_ref) << This_Ctl::e_edt_sta;
	if (edt_) {
		edt_.SetWindowText((LPCWSTR)svc_ref.State().Name());
	}

	button = Lay_(m_page_ref) << This_Ctl::e_but_sta;
	if (button) {
		button.EnableWindow(bServiceInstalled);

		if (svc_ref.State().IsRunning()) { button.SetWindowText(_T("Stop")); }
		else if (!svc_ref.State().Error()) button.SetWindowText(_T("Start"));
		else {
			button.SetWindowText(_T("Try To Start"));
		}
	}
	button = Lay_(m_page_ref) << This_Ctl::e_chk_sta;
	if (button)
		button.EnableWindow(bServiceInstalled);
#if (0)
	if (m_error)
		m_error.Show();
#endif
	return m_error;
}

HRESULT   CService_Ctrls::InitializeForWait(void) {
	m_error << __MODULE__ << S_OK;

	CWindow button = Lay_(m_page_ref) << This_Ctl::e_but_ins;
	if (button){
		button.SetWindowText(_T("Waiting..."));
		button.EnableWindow (FALSE);
	}

	::WTL::CEdit edt_;
	edt_ = Lay_(m_page_ref) << This_Ctl::e_edt_ins;
	if (edt_){
		edt_.SetWindowText(_T("Service is being deleted..."));
		edt_.EnableWindow (FALSE);
	}
	edt_ = Lay_(m_page_ref) << This_Ctl::e_edt_sta;
	if (edt_){
		edt_.SetWindowText(_T("Waiting for completion..."));
		edt_.EnableWindow (FALSE);
	}
	button = Lay_(m_page_ref) << This_Ctl::e_but_sta;
	if (button){
		button.SetWindowText(_T("Undefined"));
		button.EnableWindow(FALSE);
	}
	return m_error;
}

BOOL      CService_Ctrls::OnCommand (const WORD wCtrlId, const WORD wNotifyCode)
{
	BOOL bHandled = TRUE;  wNotifyCode;

	switch (wCtrlId) {
	case This_Ctl::e_but_ins: {
		bool bResult = false;
		const bool bServiceInstalled = CtrlMan_Object().Installed();

		if (bServiceInstalled)
			bResult = CtrlMan_Object().Uninstall();
		else
			bResult = CtrlMan_Object().Install();

		if (bResult) {
			this->Initialize();
		}
		else {
			CtrlMan_Object().Error().Show();
		}
	} break;
	case This_Ctl::e_but_sta: {
		bool bResult = false;

		const CService& svc_ref = CtrlMan_Object().GetObject();
		if (svc_ref.State().IsRunning())
			bResult = CtrlMan_Object().Stop();
		else
			bResult = CtrlMan_Object().Start();

		if (bResult) {
			this->Initialize();
		}
		else
			CtrlMan_Object().Error().Show();
	} break;
	case This_Ctl::e_chk_sta: {
		CButton button = Lay_(m_page_ref) << wCtrlId;
		if (button) {
			CService_Auto& timer_ = CSvcTimer_Object();

			bool bAutoCheck = !!button.GetCheck();
			timer_.ManageByState(bAutoCheck);
		}
	} break;
	default:
		bHandled = FALSE;
	}

	return bHandled;
}

/////////////////////////////////////////////////////////////////////////////

CService_Auto:: CService_Auto(void) : m_timer(NULL) { }
CService_Auto::~CService_Auto(void) {}

/////////////////////////////////////////////////////////////////////////////

VOID    CService_Auto::Create (const DWORD _period_ms)
{
	if (!m_owner) return;
	if ( m_timer) return;
	m_timer = m_owner.SetTimer(1, _period_ms);
}

VOID    CService_Auto::Destroy(void)
{
	if (!m_owner) return;
	if (!m_timer) return;

	m_owner.KillTimer(m_timer);
	m_timer = NULL;
}

VOID    CService_Auto::ManageByCtrl (const UINT ctrlId)
{
	if (!m_owner)
		return;
	WTL::CButton btn_ = m_owner.GetDlgItem(ctrlId);
	if (!btn_)
		return;
	const bool bAutoCheck = !!btn_.GetCheck();
	this->ManageByState(bAutoCheck);
}

VOID    CService_Auto::ManageByState(const bool _auto_check)
{
	if (!m_owner)
		return;
	if (_auto_check)
		this->Create();
	else
		this->Destroy();
}

HWND    CService_Auto::Owner(void)const { return m_owner; }

HRESULT CService_Auto::Owner(const CWindow& _owner) {
	m_owner = _owner;
	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

CService_Auto& CService_Auto::GetObject(void) { return CSvcTimer_Object(); }