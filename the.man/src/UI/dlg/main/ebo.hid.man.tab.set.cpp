/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-May-2020 at 0:57:01a, UTC+7, Novosibirsk, Wednesday;
	This is FakeGPS driver configuration dialog tab set interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo VAC driver client project on 23-Jun-2020 at 2:57:28a, UTC+7, Tuesday;
	Adopted to Ebo Pack Hide Man project on 6-Jul-2020 at 06:52:48a, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"
#include "ebo.hid.man.tab.set.h"
#include "ebo.hid.man.dlg.res.h"

using namespace ebo::hide::gui;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace hide { namespace gui { namespace _impl
{
	class CHideTabSet_Layout
	{
	private:
		RECT    m_area;
	public:
		CHideTabSet_Layout(const RECT& rcArea) : m_area(rcArea) {}

	public:
		RECT    TabsArea (void) const {
			RECT rcTabs = m_area;
			::InflateRect(&rcTabs, -5, -3);
			return rcTabs;
		}
	};
}}}}

using namespace ebo::hide::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CHideTabSet:: CHideTabSet(ITabSetCallback& _snk) : m_service(_snk) {
}
CHideTabSet::~CHideTabSet(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CHideTabSet::Create(const HWND hParent, const RECT& rcArea) {
	hParent; rcArea;
	HRESULT hr_ = S_OK;

	if(FALSE == ::IsWindow(hParent) || ::IsRectEmpty(&rcArea))
		return (hr_ = E_INVALIDARG);

	CHideTabSet_Layout layout(rcArea);
	RECT rcTabs  = layout.TabsArea();

	m_cTabCtrl.Create(
			hParent,
			rcTabs ,
			0,
			WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,
			WS_EX_CONTROLPARENT,
			IDC_EBO_HID_MAN_TAB
		);
	const SIZE szPadding = {
		5, 2
	};
	m_cTabCtrl.SetFont(::ATL::CWindow(hParent).GetFont());
	m_cTabCtrl.SetPadding(szPadding);
	m_cTabCtrl.SetItemSize(128, 24);

	INT nIndex = 0; nIndex;
	const DWORD dwFlags = SWP_NOSIZE|SWP_NOMOVE|SWP_HIDEWINDOW; // auto-placement is made by default implementation of create/initialize page handler;
	
	m_cTabCtrl.AddItem(m_service.GetPageTitle());
	{
		m_service.Create(m_cTabCtrl.m_hWnd);
		m_service.SetWindowPos(
				HWND_TOP,
				0, 0, 0 , 0,
				dwFlags
			);
		if (m_service.IsWindow()) m_service.Index(nIndex++);
	}
	m_cTabCtrl.AddItem(_T(" Hook Setup "));
	m_cTabCtrl.AddItem(_T(" Target Apps "));

	m_cTabCtrl.SetCurSel(0);
	
	this->UpdateLayout();

	return S_OK;
}

HRESULT       CHideTabSet::Destroy(void) {
	if (m_service.IsWindow()){
		m_service.DestroyWindow();  m_service.m_hWnd = NULL;
	}
	return S_OK;
}

HRESULT       CHideTabSet::UpdateData  (void) {
	m_service.UpdateData();
	return S_OK;
}

void          CHideTabSet::UpdateLayout(void)
{
	const INT nTabIndex = m_cTabCtrl.GetCurSel();

	if (m_service.IsWindow()) m_service.ShowWindow(nTabIndex == m_service.Index() ? SW_SHOW : SW_HIDE);
}