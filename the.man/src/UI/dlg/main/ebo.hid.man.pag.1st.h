#ifndef _EBOHIDEMANPAG1ST_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
#define _EBOHIDEMANPAG1ST_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-May-2020 at 3:27:56a, UTC+7, Novosibirsk, Wednesday;
	This is FakeGPS driver configuration dialog 1st page interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo VAC driver client project on 22-Jun-2020 at 7:56:05a, UTC+7, Novosibirsk, Monday;
	Adopted to Ebo Pack Hide Man project on 6-Jul-2020 at 06:37:45a, UTC+7, Novosibirsk, Monday;
*/
#include "shared.gen.sys.err.h"
#include "shared.gui.page.base.h"

namespace ebo { namespace hide { namespace gui {

	using shared::sys_core::CError;
	using shared::gui::ITabPageEvents ;
	using shared::gui::ITabSetCallback;
	using shared::gui::CTabPageBase   ;

	class CPageMan_1st : public CTabPageBase, public  ITabPageEvents {
	                    typedef CTabPageBase  TPage;
	private:
		CError          m_error  ;

	public :
		 CPageMan_1st(ITabSetCallback&);
		~CPageMan_1st(void);

	private:
#pragma warning(disable:4481)
		virtual LRESULT     OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	private: // ITabPageEvents
		virtual LRESULT     TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	public : // TPage
		virtual bool        IsChanged   (void) const override sealed;
		virtual CAtlString  GetPageTitle(void) const override sealed;
		virtual HRESULT     UpdateData  (const DWORD _opt = 0) override sealed;
#pragma warning(default:4481)
	public:
		TErrorRef           Error (void) const { return m_error; }
	};

}}}

#endif/*_EBOHIDEMANPAG1ST_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED*/