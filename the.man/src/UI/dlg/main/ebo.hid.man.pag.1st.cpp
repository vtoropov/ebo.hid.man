/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-May-2020 at 3:36:50a, UTC+7, Novosibirsk, Wednesday;
	This is FakeGPS driver configuration dialog 1st page interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo VAC driver client project on 22-Jun-2020 at 7:56:05a, UTC+7, Novosibirsk, Monday;
	Adopted to Ebo Pack Hide Man project on 6-Jul-2020 at 06:40:23a, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"
#include "ebo.hid.man.pag.1st.h"
#include "ebo.hid.man.pag.1st.def.h"
#include "ebo.hid.svc.crt.h"
#include "ebo.hid.man.pag.1st.ctl_flow.h"

using namespace ebo::hide::gui;
using namespace ebo::hide::service;

#include "shared.gui.page.layout.h"

using namespace shared::gui::layout;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace hide { namespace gui { namespace _impl
{
	class CPageMan_1st_Layout
	{
	private:
		const CWindow&   m_page_ref;
		RECT             m_page_rec;

	public:
		CPageMan_1st_Layout(const CWindow& page_ref) : m_page_ref(page_ref) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_svc_ava, This_Res::e_svc_ava) );
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_loc_ava, This_Res::e_loc_ava) );

			CWindow  svc_cap = Lay_(m_page_ref) << This_Ctl::e_svc_cap;
			if (0 != svc_cap) {
			         svc_cap.SetFont(CTabPageBase::SectionCapFont());
			}
			CWindow  loc_cap = Lay_(m_page_ref) << This_Ctl::e_loc_cap;
			if (0 != loc_cap) {
				loc_cap.SetFont(CTabPageBase::SectionCapFont());
			}
			const SIZE sz_shft = {
				0, 0
				};
			CPage_Layout(m_page_ref).AlignCtrlsByHeight(This_Ctl::e_edt_ins, This_Ctl::e_but_ins, sz_shft);
			CPage_Layout(m_page_ref).AlignCtrlsByHeight(This_Ctl::e_edt_sta, This_Ctl::e_but_sta, sz_shft);
			CPage_Layout(m_page_ref).AlignCtrlsByHeight(This_Ctl::e_edt_loc, This_Ctl::e_but_loc, sz_shft);
		}

		VOID   OnSize  (void) {
			CPage_Ban pan_(This_Ctl::e_inf_img, This_Ctl::e_inf_txt);
			pan_.Image().Resource() = This_Res::e_inf_img;
			pan_.Label().Resource() = This_Res::e_inf_txt;
			CPage_Layout(m_page_ref).AdjustBan(
				pan_
			);
		}
	};

	class CPageMan_1st_Init
	{
	private:
		CWindow&       m_page_ref;
		CError         m_error   ;
		
	public:
		CPageMan_1st_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		TErrorRef Error (void) const { return m_error; }
		HRESULT   OnCreate(void) {
			m_error << __MODULE__ << S_OK;
			// https://docs.microsoft.com/en-us/windows/win32/api/mmeapi/nf-mmeapi-waveingetdevcaps
			Default ();
			CService_Ctrls svc_ctrls(m_page_ref);
			HRESULT hr_ =  svc_ctrls.Initialize();
			if (FAILED(hr_)) {
				m_error = svc_ctrls.Error();
				OnUpdate(m_error);
			}
			else
				OnUpdate();
			return hr_;
		}
		HRESULT   OnUpdate(void) {
			CWindow inf_txt = Lay_(m_page_ref) << This_Ctl::e_inf_txt;
			if (0!= inf_txt) {
				CAtlString cs_inf; cs_inf.LoadString(This_Res::e_inf_txt);
				inf_txt.SetWindowText( (LPCTSTR) cs_inf );
			}
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_inf_img, This_Res::e_inf_img) );
			return S_OK;
		}

		HRESULT   OnUpdate(TErrorRef _err) {
			CWindow inf_txt = Lay_(m_page_ref) << This_Ctl::e_inf_txt;
			if (0!= inf_txt) {
				CAtlString cs_err;
				CAtlString cs_pat; cs_pat.LoadStringW(This_Res::e_err_txt);
				cs_err.Format(cs_pat.GetString(), _err.Desc());

				inf_txt.SetWindowText( (LPCTSTR) cs_err );
			}
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_inf_img, This_Res::e_err_img) );
			return S_OK;
		}

	private:
		HRESULT   Default (void) {
			m_error << __MODULE__ << S_OK;

			CWindow edt_ins = Lay_(m_page_ref) << This_Ctl::e_edt_ins; if (NULL != edt_ins) { edt_ins.SetWindowTextW(_T("Not Installed")); }
			CWindow edt_sta = Lay_(m_page_ref) << This_Ctl::e_edt_sta; if (NULL != edt_sta) { edt_sta.SetWindowTextW(_T("Not Defined")); }
			CWindow edt_loc = Lay_(m_page_ref) << This_Ctl::e_edt_loc; if (NULL != edt_loc) { edt_loc.SetWindowTextW(CSvcCrt().PathShortcut().GetString()); }

			CWindow btn_browse = Lay_(m_page_ref) << This_Ctl::e_but_loc;
			if (btn_browse)
				btn_browse.EnableWindow(FALSE);

			return m_error;
		}
	};

	class CPageMan_1st_Handler {
	private:
		CWindow&         m_page_ref;

	public:
		CPageMan_1st_Handler(CWindow& dlg_ref) : m_page_ref(dlg_ref) {}

	public:
		BOOL   OnCommand (const WORD u_ctl_id, const WORD u_ntf_cd) {
			u_ntf_cd; u_ctl_id;

			BOOL bHandled = TRUE;
			switch (u_ctl_id) {
			case This_Ctl::e_but_ins: {
				CService_Ctrls svc_ctrls(m_page_ref); bHandled = svc_ctrls.OnCommand(u_ctl_id, u_ntf_cd);
				} break;
			default:
				bHandled = FALSE;
			}
			return bHandled;
		}
		BOOL   OnNotify  (WPARAM _w_p, LPARAM _l_p) {
			_w_p; _l_p;
			BOOL    bHandled = FALSE ;
			return (bHandled = FALSE);
		}
	};
}}}}
using namespace ebo::hide::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPageMan_1st:: CPageMan_1st(ITabSetCallback& _set_snk) :
       TPage(IDD_EBO_HID_MAN_1ST_PAG, *this, _set_snk) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CPageMan_1st::~CPageMan_1st(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageMan_1st::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	const LRESULT l_res = TPage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPageMan_1st::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TPage::OnPageInit(uMsg, wParam, lParam, bHandled);

	CPageMan_1st_Layout layout_(*this);
	layout_.OnCreate();

	CPageMan_1st_Init init_(*this);
	HRESULT hr_ = init_.OnCreate();
	if (FAILED(hr_))
		m_error = init_.Error();

	TPage::m_set_snk.TabSet_OnDataChanged(TPage::Index(), FALSE);
	TPage::m_bInited = true;
	return l_res;
}

LRESULT    CPageMan_1st::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TPage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPageMan_1st_Layout layout_(*this);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageMan_1st::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
			if (!TPage::m_bInited)
				break;
			const WORD u_ntf_cd = HIWORD(wParam);
			const WORD u_ctl_id = LOWORD(wParam);

			CPageMan_1st_Handler handler_(*this);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(u_ctl_id, u_ntf_cd);
			if (bHandled == TRUE ) {
				TPage::m_set_snk.TabSet_OnDataChanged(TPage::Index(), FALSE);
			}
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPageMan_1st::IsChanged   (void) const {
	return false;
}

CAtlString CPageMan_1st::GetPageTitle(void) const {  static CAtlString cs_title(_T(" Service ")); return cs_title; }

HRESULT    CPageMan_1st::UpdateData  (const DWORD _opt) {
	m_error << __MODULE__ << S_OK; _opt;
	return m_error;
}