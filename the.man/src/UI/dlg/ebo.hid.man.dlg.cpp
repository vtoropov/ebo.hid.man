/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-May-2020 at 0:35:24a, UTC+7, Novosibirsk, Wednesday;
	This is FakeGPS driver configuration dialog interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo VAC driver client project on 22-Jun-2020 at 4:46:35p, UTC+7, Monday;
	Adopted to Ebo Pack Hide Man project on 6-Jul-2020 at 07:02:33a, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"
#include "ebo.hid.man.dlg.h"
#include "ebo.hid.man.dlg.res.h"

using namespace ebo::hide::gui;

#include "shared.gui.page.layout.h"

using namespace shared::gui::layout;

#include "shared.gen.app.res.h"
#include "shared.uix.gdi.provider.h"

using namespace shared::user32;
using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace hide { namespace gui { namespace _impl {

	class CHideManDlg_Ctl {
	public:
		enum _ctl : WORD {
			e_ctl_ban   = IDC_EBO_HID_MAN_BAN  ,
			e_ctl_sep   = IDC_EBO_HID_MAN_SEP  ,
			e_ctl_set   = IDOK                 ,
			e_ctl_apply = IDYES                , 
			e_ctl_close = IDCANCEL  
		};
	};
	typedef CHideManDlg_Ctl This_Ctl;

	class CHideManDlg_Res {
	public:
		enum _res : WORD {
			e_res_ban   = IDR_EBO_HID_MAN_BAN  ,
		};
	};
	typedef CHideManDlg_Res This_Res;

	class CHideManDlg_Initer {
	private:
		CWindow&     m_dlg_ref;

	public:
		 CHideManDlg_Initer (CWindow&  _dlg_ref) : m_dlg_ref(_dlg_ref) {}
		~CHideManDlg_Initer (void) {}

	public:
		HRESULT      OnCreate(void) {
			HRESULT hr_ = S_OK;
			return  hr_;
		}
	};

	class CHideManDlg_Layout {
	private:
		enum _e {
			e_gap = 0x04,
			e_ban = 0x4b,  // 75x850px is taken from PNG banner file; this is a hight of the banner;
		};

	private:
		CWindow&     m_dlg_ref;
		RECT         m_dlg_rec;

	public:
		CHideManDlg_Layout(CWindow&  _dlg_ref) : m_dlg_ref(_dlg_ref) {

			if (m_dlg_ref.GetClientRect(&m_dlg_rec) == FALSE)
				::SetRectEmpty(&m_dlg_rec);
		}

	public:
		RECT     BanArea(VOID) {
			__no_args;
			RECT rc_ban  = {0};
			CWindow ban_ = (Lay_(m_dlg_ref) << This_Ctl::e_ctl_ban);
			if (NULL == ban_)
				return rc_ban;

			rc_ban = (Lay_(m_dlg_ref) = This_Ctl::e_ctl_ban);
			return rc_ban;
		}

		RECT     TabArea(VOID) {
			const
			RECT rc_ban = this->BanArea();
			RECT rc_sep = (Lay_(m_dlg_ref) = This_Ctl::e_ctl_sep);

			RECT rc_tab = {0};
			::SetRect(
				&rc_tab, m_dlg_rec.left + _e::e_gap, rc_ban.bottom - _e::e_gap/2, m_dlg_rec.right - _e::e_gap, rc_sep.top - _e::e_gap
			);

			return rc_tab;
		}
	};

	class CHideManDlg_Handler {
	private:
		CWindow&     m_dlg_ref;

	public:
		CHideManDlg_Handler(CWindow&  _dlg_ref) : m_dlg_ref(_dlg_ref) {}

	public:
		VOID   OnCommand(WORD wNotify, WORD ctrlId) {
			wNotify; ctrlId;
#if (0)
			switch (ctrlId) {
			case This_Ctl::e_ctl_apply: {} break;
			case This_Ctl::e_ctl_set  : {} break;
			case This_Ctl::e_ctl_close:
				::EndDialog(m_dlg_ref, ctrlId); break;
			}
#endif
		}
	};

}}}}
using namespace ebo::hide::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CHideManDlg::CHideManDlgImpl:: CHideManDlgImpl(void) : 
	IDD(IDD_EBO_HID_MAN_DLG), m_banner(This_Res::e_res_ban), m_tabset(*this) {
	m_banner.Renderer(this);	
}
CHideManDlg::CHideManDlgImpl::~CHideManDlgImpl(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CHideManDlg::CHideManDlgImpl::OnBtnCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const WORD wNotify = HIWORD(wParam); wNotify;
	const WORD ctrlId  = LOWORD(wParam); ctrlId ;

	CHideManDlg_Handler hand_(*this);
	hand_.OnCommand(wNotify, ctrlId);

	switch (ctrlId) {
	case This_Ctl::e_ctl_apply: { m_tabset.UpdateData(); } break;
	case This_Ctl::e_ctl_set  : { m_tabset.UpdateData(); } 
	case This_Ctl::e_ctl_close:
	     TDialog::EndDialog(ctrlId); break;
	}

	return 0;
}

LRESULT   CHideManDlg::CHideManDlgImpl::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled = FALSE;
	m_tabset.Destroy();
	m_banner.Destroy();
	return 0;
}

LRESULT   CHideManDlg::CHideManDlgImpl::OnInitDlg(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	TDialog::CenterWindow();
	{
		CIconLoader   loader_(IDR_EBO_HID_MAN_DLG_ICO);
		TDialog::SetIcon(loader_.DetachLarge(), TRUE );
		TDialog::SetIcon(loader_.DetachSmall(), FALSE);
	}
	HRESULT hr_ = m_banner.Create(TDialog::m_hWnd, This_Ctl::e_ctl_ban);
	if (SUCCEEDED(hr_)){}
	CHideManDlg_Initer   initer_(*this); initer_.OnCreate();

	CHideManDlg_Layout   layout_(*this);
	const RECT rc_tabs = layout_.TabArea();

	hr_ = m_tabset.Create(*this, rc_tabs);

	return 0;
}

LRESULT   CHideManDlg::CHideManDlgImpl::OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	switch (wParam)  {
	case VK_RETURN : {
			CWindow sav_but = (Lay_(*this)) << This_Ctl::e_ctl_apply;
			if (sav_but.IsWindowEnabled()) {
				CHideManDlg_Handler hand_(*this); hand_.OnCommand(0, This_Ctl::e_ctl_apply);
			}
		} break;
	}
	return 0;
}

LRESULT   CHideManDlg::CHideManDlgImpl::OnSysCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam){
	case SC_CLOSE: {
			TDialog::EndDialog(IDCANCEL);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CHideManDlg::CHideManDlgImpl::OnNotify (INT idCtrl, LPNMHDR pnmh, BOOL& bHandled) {
	idCtrl; pnmh; bHandled = TRUE;
	m_tabset.UpdateLayout();
	return 0;
}

/////////////////////////////////////////////////////////////////////////////
#if (1)
HRESULT   CHideManDlg::CHideManDlgImpl::TabSet_OnDataChanged (const UINT pageId, const bool bChanged ) {
	pageId; bChanged;
	HRESULT hr_ = S_OK;

	CWindow app_but = (Lay_(*this)) << This_Ctl::e_ctl_apply;
	if (NULL != app_but) {
		app_but.EnableWindow(bChanged);
	}
	CWindow set_but = (Lay_(*this)) << This_Ctl::e_ctl_set;
	if (NULL != set_but) {
		set_but.EnableWindow(bChanged);
	}

	return  hr_;
}

HRESULT   CHideManDlg::CHideManDlgImpl::TabSet_OnDataRequest (const UINT ctrlId, RECT& _rc_ctrl) {
	ctrlId; _rc_ctrl;
	HRESULT hr_ = S_OK;
	switch (ctrlId) {
	case IDYES   : case IDOK:
	case IDCANCEL:
	{
		CWindow ctl_btn = TDialog::GetDlgItem(ctrlId);
		if (NULL != ctl_btn) {
			ctl_btn.GetWindowRect(&_rc_ctrl);
		}
	} break;
	default:
	hr_ = DISP_E_PARAMNOTFOUND;
	}
	return  hr_;
}
#endif
/////////////////////////////////////////////////////////////////////////////

HRESULT   CHideManDlg::CHideManDlgImpl::DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& _rc_area) const {
	hChild;
	if (NULL == hSurface)
		return __DwordToHresult(ERROR_INVALID_HANDLE);
	if (::IsRectEmpty(&_rc_area))
		return OLE_E_INVALIDRECT;
	// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getsyscolor
	CZBuffer dc_(hSurface, _rc_area);
	dc_.FillSolidRect(
		&_rc_area, ::GetSysColor(COLOR_3DFACE)
	);
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CHideManDlg:: CHideManDlg(void) {}
CHideManDlg::~CHideManDlg(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CHideManDlg::DoModal(void) {

	const INT_PTR result = m_dlg.DoModal();
	switch (result) {
	case IDNO : return S_OK    ;
	case IDOK : return S_OK    ;
	}
	return S_FALSE;
}