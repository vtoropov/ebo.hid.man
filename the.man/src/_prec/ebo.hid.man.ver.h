#ifndef _EBOSVCGOOPANVER_H_6ECEFB93_ABCD_4677_85B0_CEFB85902C6D_INCLUDED
#define _EBOSVCGOOPANVER_H_6ECEFB93_ABCD_4677_85B0_CEFB85902C6D_INCLUDED
/*
	The following macros define the minimum required platform. The minimum required platform
	is the earliest version of Windows, Internet Explorer etc. that has the necessary features to run 
	your application. The macros work by enabling all features available on platform versions up to and 
	including the version specified.

	Created by Tech_dog (ebontrop@gmail.com) on 21-May-2019 at 6:16:29p, UTC+7, Phuket, Rawai, Tuesday;
	This is communication data exchange receiver desktop console application version declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack virtual printer console on 5-Sep-2019 at 3:02:45p, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Game Dyna Inject project on 27-Feb-2020 at 3:24:20a, UTC+7, Novosibirska, Tuesday;
	Adopted to FakeGPS driver project on 16-Apr-2020 at 2:55:16p, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Xor Java Eclipse build tool project on 21-May-2020 at 5:31:55p, UTC+7, Thursday;
	Adopted to Ebo VAC driver client project on 22-Jun-2020 at 6:58:36a, UTC+7, Monday;
	Adopted to Ebo Pack Hide Man project on 6-Jul-2020 at 05:52:06a, UTC+7, Novosibirsk, Monday;
*/

#ifndef WINVER                 // Specifies that the minimum required platform is Windows Vista.
#define WINVER         0x0600  // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT           // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINNT   0x0600  // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINDOWS         // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINDOWS 0x0600  // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE              // Specifies that the minimum required platform is Internet Explorer 9.0 (Windows Vista, service pack 2).
#define _WIN32_IE      0x0900  // Change this to the appropriate value to target other versions of IE.
#endif

#endif/*_EBOSVCGOOPANVER_H_6ECEFB93_ABCD_4677_85B0_CEFB85902C6D_INCLUDED*/