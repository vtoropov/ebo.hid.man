#ifndef _FGCOMMONDEFS_H_AE1D4539_8DA4_4b69_B114_F653D9B0D394_INCLUDED
#define _FGCOMMONDEFS_H_AE1D4539_8DA4_4b69_B114_F653D9B0D394_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Feb-2016 at 12:50:51am, GMT+7, Phuket, Rawai, Saturday;
	This is File Guardian generic shared definitions declaration file.
	( project: https://thefileguardian.com )
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Hide Man project on 13-Jul-2020 at 4:40:53p, UTC+7, Novosibirsk, Monday;
*/
namespace ebo { namespace hide { namespace service
{
	class CModuleNames {
	protected:
		CAtlStringW    m_exec;
		CAtlStringW    m_name;
		HRESULT        m_result;
	protected:
		CModuleNames(void);
	public:
		LPCWSTR        Executable(void) const;
		HRESULT        LastResult(void) const;
		LPCWSTR        Name(void) const;
	};

	class CServiceNames : public CModuleNames {
	                     typedef CModuleNames TBase;
	public:
		CServiceNames(void);
	};
}}}

#endif/*_FGCOMMONDEFS_H_AE1D4539_8DA4_4b69_B114_F653D9B0D394_INCLUDED*/
