#ifndef _EBOHIDSVCCFG_H_9CE142DC_4E0F_4FBB_A9E0_1B4DE4F703E2_INCLUDED
#define _EBOHIDSVCCFG_H_9CE142DC_4E0F_4FBB_A9E0_1B4DE4F703E2_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Jul-2020 at 12:53:08p, UTC+7, Novosibirsk, Wednesday;
	This is Ebo Pack Hide Man target app processes monitor service configuration interface declaration file.
*/
#include "shared.reg.hive.h"
#include "sys.svc.error.h"

namespace ebo { namespace hide { namespace cfg {

	using shared::service::CSvcError;
	using shared::registry::CRegistryStg;

	class CSvc_Cfg {
	private:
		CSvcError    m_error;
		CRegistryStg m_stg  ;
		CAtlStringW  m_path ;
		bool         m_auto ;

	public:
		 CSvc_Cfg (void);
		~CSvc_Cfg (void);

	public:
		bool      Auto (void) const;           // gets service state auto-check mode;
		bool&     Auto (void)      ;           // sets service state auto-check mode;
		LPCWCHAR  Path (void) const;           // gets a service executable file path;
		HRESULT   Path (LPCWCHAR _lp_sz_path); // sets a service executable file path;

	public:
		TErrorRef    Error (void) const;
		HRESULT      Load  (void)      ;
		HRESULT      Save  (void)      ;
	};
}}}

typedef ebo::hide::cfg::CSvc_Cfg  TSvcCfg;

#endif/*_EBOHIDSVCCFG_H_9CE142DC_4E0F_4FBB_A9E0_1B4DE4F703E2_INCLUDED*/