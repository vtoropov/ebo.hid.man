#ifndef _FGSVCMANAGER_H_D465BA32_9E31_44b4_BCE0_96ADD39FB95F_INCLUDED
#define _FGSVCMANAGER_H_D465BA32_9E31_44b4_BCE0_96ADD39FB95F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Feb-2016 at 10:12:57pm, GMT+7, Phuket, Rawai, Tuesday;
	This is File Guardian Background Service Manager class declaration file.
	( project: https://thefileguardian.com )
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Hide Man project on 13-Jul-2020 at 5:23:01p, UTC+7, Novosibirsk, Monday;
*/
#include "sys.svc.error.h"
#include "sys.svc.create.h"
#include "sys.svc.man.h"

#include "ebo.hid.svc.crt.h"

namespace ebo { namespace hide { namespace service {

	using shared::sys_core::CError;

	using shared::service::CSvcError ;
	using shared::service::CSvcState ;
	using shared::service::CService  ;
	using shared::service::CCrtData  ;
	using shared::service::CCrtOption;
	using shared::service::CManager  ;

	class CControlManager {
	private:
		mutable
		CSvcError  m_error;
		CManager   m_mgr;
		CCrtData   m_crt;
		CService   m_svc;
	public:
		 CControlManager(void);
		 CControlManager(const CAtlStringW& _exe_path);   // this constructor for installation procedure;
		~CControlManager(void);

	public:
		TErrorRef  Error(void) const;
		bool       Install    (void);
		bool       Installed  (void);
		const
		CService&  GetObject  (void);
		bool       Start      (void);
		bool       Stop       (void);
		bool       Uninstall  (void);
	};
}}}

#endif/*_FGSVCMANAGER_H_D465BA32_9E31_44b4_BCE0_96ADD39FB95F_INCLUDED*/