#ifndef _FGSVCCRTDATA_H_500390B1_5C1D_4882_9801_47F4CA2F89DF_INCLUDED
#define _FGSVCCRTDATA_H_500390B1_5C1D_4882_9801_47F4CA2F89DF_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Jan-2016 at 5:59:07pm, GMT+7, Phuket, Rawai, Monday;
	This is File Guardian background service create data class(es) declaration file.
	( project: https://thefileguardian.com )
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Hide Man project on 13-Jul-2020 at 3:23:53p, UTC+7, Novosibirsk, Monday;
*/
#include "sys.svc.create.h"

namespace ebo { namespace hide { namespace service
{
	using shared::service::CCrtData;
	using shared::service::CCrtOption;

	HRESULT InitializeCrtData(CCrtData&);

	class CSvcCrt : public CCrtData {
	               typedef CCrtData TCrt;
	public:
		 CSvcCrt (void);
		~CSvcCrt (void);

	public:
		CAtlStringW PathShortcut(void) const;
	};
}}}

#endif/*_FGSVCCRTDATA_H_500390B1_5C1D_4882_9801_47F4CA2F89DF_INCLUDED*/