/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Jul-2020 at 1:03:46p, UTC+7, Novosibirsk, Wednesday;
	This is Ebo Pack Hide Man target app processes monitor service configuration interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.hid.svc.cfg.h"

using namespace ebo::hide::cfg;
using namespace shared::registry;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace hide { namespace cfg { namespace _impl {

	class CReg_Folder {
	public:
		static LPCWSTR Root(void) { static LPCWSTR lp_sz_fld = L"Software\\Hide"; return lp_sz_fld; }
	};
	class CReg_Service : public CReg_Folder {
	public:
		LPCWSTR Folder (void) const {
			static CAtlStringW cs_in; if (cs_in.IsEmpty()) {
				cs_in += CReg_Folder::Root();
				cs_in += _T("Service");
			}
			return (LPCWSTR)cs_in;
		}
	public:
		LPCWSTR Auto (void) const {
			static LPCWSTR lp_sz_value = L"Check"; return lp_sz_value;
		}
		
		LPCWSTR Path (void) const {
			static LPCWSTR lp_sz_value = L"Path"; return lp_sz_value;
		}
	};

}}}}
using namespace ebo::hide::cfg::_impl;

/////////////////////////////////////////////////////////////////////////////

CSvc_Cfg:: CSvc_Cfg(void) : m_stg(HKEY_CURRENT_USER, CRegOptions::eDoNotModifyPath), m_auto(false) {
	m_error << __FUNCTIONW__ << S_OK >> __FUNCTIONW__; }
CSvc_Cfg::~CSvc_Cfg(void) {}

/////////////////////////////////////////////////////////////////////////////

bool       CSvc_Cfg::Auto (void) const { return m_auto; }
bool&      CSvc_Cfg::Auto (void)       { return m_auto; }
LPCWCHAR   CSvc_Cfg::Path (void) const { return m_path.GetString(); }
HRESULT    CSvc_Cfg::Path (LPCWCHAR _lp_sz_path) {
	m_error << __FUNCTIONW__ << S_OK;

	if (NULL == _lp_sz_path || 0 == ::wcslen(_lp_sz_path))
		return (m_error << E_INVALIDARG);

	m_path = _lp_sz_path;

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef  CSvc_Cfg::Error (void) const { return m_error; }
HRESULT    CSvc_Cfg::Load  (void) {
	m_error << __FUNCTIONW__ << S_OK;

	HRESULT hr_ = S_OK;

	CAtlStringW  cs_path;
	LONG  l_auto = 0;

	if (SUCCEEDED(hr_)) hr_ = this->m_stg.Load(CReg_Service().Folder(), CReg_Service().Path(), cs_path );
	if (SUCCEEDED(hr_))
	{ m_path = cs_path; }
	else
		m_error = m_stg.Error();

	if (SUCCEEDED(hr_)) hr_ = this->m_stg.Load(CReg_Service().Folder(), CReg_Service().Auto(), l_auto );
	if (SUCCEEDED(hr_))
	{ this->Auto() = l_auto!=0; }
	else
		m_error = m_stg.Error();

	return m_error;
}

HRESULT    CSvc_Cfg::Save  (void)  {
	m_error << __FUNCTIONW__ << S_OK;

	if (m_path.IsEmpty())
		return (m_error << OLE_E_BLANK);

	HRESULT hr_ = this->m_stg.Save(CReg_Service().Folder(), CReg_Service().Path(), m_path );
	if (SUCCEEDED(hr_)) {}
	else m_error = this->m_stg.Error();

	hr_ = this->m_stg.Save(CReg_Service().Folder(), CReg_Service().Auto(), static_cast<LONG>(this->Auto()) );
	if (SUCCEEDED(hr_)) {}
	else m_error = this->m_stg.Error();

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////