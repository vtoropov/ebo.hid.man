/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 2:53:53am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian Background Service Shared Library precompiled headers implementation file.
	( project: https://thefileguardian.com )
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Hide Man project on 13-Jul-2020 at 2:55:44p, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)