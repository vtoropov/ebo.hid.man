/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Jan-2016 at 6:05:33pm, GMT+7, Phuket, Rawai, Monday;
	This is File Guardian background service create data class(es) implementation file.
	( project: https://thefileguardian.com )
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Hide Man project on 13-Jul-2020 at 3:29:11p, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"
#include "ebo.hid.svc.crt.h"
#include "ebo.hid.svc.names.h"

using namespace ebo::hide::service;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace hide { namespace service
{

	HRESULT    InitializeCrtData(CCrtData& _crt)
	{
		CServiceNames svc_names;

		_crt.Account()     = _T("NT AUTHORITY\\LocalSystem");
		_crt.Description() = _T("Ebo Pack hide process service is for monitoring specific process(es) creation;");
		_crt.DisplayName() = _T("Ebo Pack hide process service;");
		_crt.Name()        = svc_names.Name();
		_crt.Path()        = svc_names.Executable();
		_crt.Options()     = (CCrtOption::eStoppable | CCrtOption::eCanShutdown);

		return  svc_names.LastResult();
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CSvcCrt:: CSvcCrt(void) : TCrt() { InitializeCrtData(*this); }
CSvcCrt::~CSvcCrt(void) {}

/////////////////////////////////////////////////////////////////////////////

CAtlStringW CSvcCrt::PathShortcut(void) const { return CAtlStringW(_T("$(ApplicationFolder)\\$(ServiceName)"));
}