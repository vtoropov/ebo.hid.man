/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Feb-2016 at 10:27:11pm, GMT+7, Phuket, Rawai, Tuesday;
	This is File Guardian Background Service Manager class implementation file.
	( project: https://thefileguardian.com )
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Hide Man project on 13-Jul-2020 at 5:42:58p, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"
#include "ebo.hid.svc.ctl.h"

using namespace ebo::hide::service;
using namespace shared::service;

/////////////////////////////////////////////////////////////////////////////

CControlManager:: CControlManager(void) { m_error << __FUNCTIONW__ << S_OK >> __FUNCTIONW__;

	HRESULT hr_ = InitializeCrtData(m_crt);
	if (FAILED(hr_))
		m_error << (hr_);
	else {
		hr_ = m_mgr.Initialize(CManagerParams::eOpenForService);
		if (FAILED(hr_))
			m_error = m_mgr.Error();
	}
}

CControlManager:: CControlManager(const CAtlStringW& _exe_path) { m_error << __FUNCTIONW__ << S_OK >> __FUNCTIONW__;
	
	HRESULT hr_ = InitializeCrtData(m_crt);
	if (FAILED(hr_))
		m_error << (hr_);
	else
	{
		m_crt.Path() = _exe_path;
		hr_ = m_mgr.Initialize(CManagerParams::eOpenForService);
		if (FAILED(hr_))
			m_error = m_mgr.Error();
	}
}
CControlManager::~CControlManager(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef CControlManager::Error    (void) const { return m_error; }

bool      CControlManager::Install  (void) {
	m_error << __FUNCTIONW__ << S_OK;

	if (m_mgr.IsValid() == false) {
		m_error.State().Set(
			__DwordToHresult(ERROR_INVALID_STATE), _T("Service manager is not initialized;")
		);
		return false;
	}

	HRESULT hr_ = m_mgr.Create(m_crt, m_svc);
	if (FAILED(hr_))
		m_error = m_mgr.Error();

	return (!m_error);
}

bool      CControlManager::Installed(void) {
	m_error << __FUNCTIONW__ << S_OK;
	if (m_mgr.IsValid() == false) {
		m_error.State().Set(
			__DwordToHresult(ERROR_INVALID_STATE), _T("Service manager is not initialized;")
		);
		return false;
	}

	HRESULT hr_ = m_mgr.Open(m_crt, CServiceAccessLevel::eQueryStatus, m_svc);
	if (FAILED(hr_)){
		m_error = m_mgr.Error();
		return false;
	}

	hr_ = m_svc.UpdateCurrentState();
	if (FAILED(hr_)) {
		m_error = m_mgr.Error();
		return false;
	}

	return m_error;
}
const
CService& CControlManager::GetObject(void)
{
	HRESULT hr_ = m_mgr.Open(m_crt, CServiceAccessLevel::eQueryStatus, m_svc);
	if (SUCCEEDED(hr_))
		hr_ = m_svc.UpdateCurrentState();

	if (!m_svc.State().Error())
	     m_svc.State().Error() = hr_;

	if (FAILED(hr_))
		m_svc.State().Error().Source(__FUNCTIONW__);
	
	return m_svc;
}

bool      CControlManager::Start (void)
{
	if (m_mgr.IsValid() == true)
		return false;

	m_error.Module(__FUNCTIONW__);
	m_error = S_OK;

	const CAtlStringW cs_name = m_crt.Name();

	HRESULT hr_ = m_mgr.Start(cs_name);
	if (FAILED(hr_))
		m_error = m_mgr.Error();

	return (!m_error);
}

bool      CControlManager::Stop  (void)
{
	if (m_mgr.IsValid() == false)
		return false;

	const CAtlStringW cs_name = m_crt.Name();

	HRESULT hr_ = m_mgr.Stop(cs_name);
	if (FAILED(hr_))
		m_error = m_mgr.Error();

	return (!m_error);
}

bool      CControlManager::Uninstall(void)
{
	if (m_mgr.IsValid() == false)
		return false;

	m_error.Module(__FUNCTIONW__);
	m_error = S_OK;

	const CAtlStringW cs_name = m_crt.Name();

	HRESULT hr_ = m_mgr.Remove(cs_name);
	if (FAILED(hr_))
		m_error = m_mgr.Error();

	return (!m_error);
}