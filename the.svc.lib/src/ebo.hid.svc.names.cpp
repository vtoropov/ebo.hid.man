/*
	Created by Tech_dog (VToropov) on 29-Dec-2017 at 5:48:17am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian generic shared definitions implementation file.
	( project: https://thefileguardian.com )
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Hide Man project on 13-Jul-2020 at 4:48:35p, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"
#include "ebo.hid.svc.names.h"

using namespace ebo::hide::service;

#include "shared.gen.app.obj.h"

using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

CModuleNames::CModuleNames(void) : m_result(OLE_E_BLANK) {}

/////////////////////////////////////////////////////////////////////////////

LPCWSTR    CModuleNames::Executable(void) const { return m_exec.GetString(); }
HRESULT    CModuleNames::LastResult(void) const { return m_result;     }
LPCWSTR    CModuleNames::Name(void) const { return m_name.GetString(); }

/////////////////////////////////////////////////////////////////////////////

CServiceNames::CServiceNames(void) {
	//
	// the service executable name must be consistent with application bitness;
	// otherwise, it will cause system error during ::StartService command: 'The system cannot find the file specified.'
	//
	// the other important part is right service executable name, if name is changed, it also can
	// lead to system error as described above;
	//
	TBase::m_name = _T("ebo.hid.svc_v15");

	CAtlString cs_path;
	TBase::m_result = GetAppObjectRef().GetPathFromAppFolder(_T(".\\"), cs_path);
	if (SUCCEEDED(TBase::m_result))
	{
		TBase::m_exec.Format(
			_T("%s.exe"), TBase::m_name.GetString()
			);
		cs_path      += TBase::m_exec;
		TBase::m_exec = cs_path;
	}
}