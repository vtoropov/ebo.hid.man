/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jul-2020 at 2:09:28p, UTC+7, Novosibirsk, Thursday;
	This is Ebo Pack shared process hook entry point module implementation file.
*/
#include "StdAfx.h"
#include "shared.hook.reg.stg.h"
#include "shared.fun.psapi.h"

/////////////////////////////////////////////////////////////////////////////

BOOL WINAPI DllMain(HINSTANCE hModule, DWORD ul_reason_for_call, LPVOID lpReserved) {
	hModule; lpReserved;

	switch (ul_reason_for_call) {
	case DLL_PROCESS_ATTACH: {
#ifdef _DEBUG
		 ::MessageBoxA(
			HWND_DESKTOP, "Debug mode entry point...", "Debug", MB_ICONEXCLAMATION|MB_OK
		);
#endif
		ebo::hide::functors::psapi::CFunctor ps_fun;
		HRESULT hr_ = S_OK;
		if (SUCCEEDED(hr_)) hr_ = ps_fun.Load();
		if (SUCCEEDED(hr_)) hr_ = ps_fun.Replace();

	} break;

	case DLL_THREAD_ATTACH :
	case DLL_THREAD_DETACH :
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}