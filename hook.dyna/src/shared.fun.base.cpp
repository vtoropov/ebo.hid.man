/*
	Created by Tech_dog (ebontrop@gmail.com) on 7-Jul-2020 at 11:14:33a, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Pack functor usage base interface implementation file.
*/
#include "StdAfx.h"
#include "shared.fun.base.h"

using namespace ebo::hide::functors;

/////////////////////////////////////////////////////////////////////////////

CFunctor_Base:: CFunctor_Base(void) { m_error << __FUNCTIONW__ << S_OK >> __FUNCTIONW__; ::ZeroMemory(&m_info, sizeof(MODULEINFO)); }
CFunctor_Base::~CFunctor_Base(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorExRef CFunctor_Base::Error  (void) const { return m_error; }
const bool  CFunctor_Base::Is     (void) const { return (NULL != m_info.EntryPoint && NULL != m_info.lpBaseOfDll && 0 != m_info.SizeOfImage); }
HRESULT     CFunctor_Base::Load   (LPCSTR _lp_sz_lib_name) {
	m_error << __FUNCTIONW__ << S_OK;

	if (NULL == _lp_sz_lib_name || 0 == ::strlen(_lp_sz_lib_name))
		return (m_error << E_INVALIDARG);

	if (this->Is() == true)
		return (m_error << __DwordToHresult(ERROR_ALREADY_INITIALIZED));

	// https://docs.microsoft.com/en-us/windows/win32/api/psapi/nf-psapi-getmoduleinformation
	if (FALSE == ::GetModuleInformation(
		::GetCurrentProcess(), ::GetModuleHandle(NULL), &m_info, sizeof(MODULEINFO))
	) return ((CError&)m_error = ::GetLastError());

	// gets import directory pointer;
	LPBYTE const pAddress = reinterpret_cast<LPBYTE>(m_info.lpBaseOfDll);
	if ( NULL == pAddress )
		return (m_error << __DwordToHresult(ERROR_INVALID_DATA));

	PIMAGE_DOS_HEADER pDos = reinterpret_cast<PIMAGE_DOS_HEADER>(pAddress);
	if ( NULL == pDos )
		return (m_error << __DwordToHresult(ERROR_BAD_FORMAT));

	PIMAGE_NT_HEADERS p_nt_head = reinterpret_cast<PIMAGE_NT_HEADERS>(pAddress + pDos->e_lfanew);
	if ( NULL == p_nt_head )
		return (m_error << __DwordToHresult(ERROR_BAD_FORMAT));

	PIMAGE_OPTIONAL_HEADER p_opts = &p_nt_head->OptionalHeader;

	this->m_import = reinterpret_cast<PIMAGE_IMPORT_DESCRIPTOR>(pAddress + p_opts->DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
	if (  m_import == NULL )
		( m_error << __DwordToHresult(ERROR_INVALID_IMPORT_OF_NON_DLL));

	bool b_found = false;

	for (; m_import->Characteristics; m_import++) {
		b_found = 0 == ::_stricmp(_lp_sz_lib_name, reinterpret_cast<LPCSTR>(pAddress + m_import->Name));
		if (b_found == true)
			break;
	}

	if (b_found ==  false)
		m_error << TYPE_E_ELEMENTNOTFOUND;
		
	return m_error;
}

HRESULT     CFunctor_Base::Replace(LPCSTR _lp_sz_fun_name, PVOID const _p_new_fun) {
	m_error << __FUNCTIONW__ << S_OK;

	if (m_import == NULL || false == this->Is())
		return (m_error << OLE_E_BLANK);

	if (NULL == _lp_sz_fun_name || 0 == ::strlen(_lp_sz_fun_name))
		return (m_error << E_INVALIDARG);

	if (NULL == _p_new_fun)
		return (m_error << E_POINTER);

	LPBYTE const pAddress = reinterpret_cast<LPBYTE>(m_info.lpBaseOfDll);
	if ( NULL == pAddress )
		return (m_error << __DwordToHresult(ERROR_INVALID_DATA));

	PIMAGE_THUNK_DATA pOrigin = reinterpret_cast<PIMAGE_THUNK_DATA>(pAddress + m_import->OriginalFirstThunk);
	PIMAGE_THUNK_DATA pFirst  = reinterpret_cast<PIMAGE_THUNK_DATA>(pAddress + m_import->FirstThunk);

	if (NULL == pOrigin || NULL == pFirst)
		return (m_error << E_POINTER);

	bool b_found = false;

	for (; !(pOrigin->u1.Ordinal & IMAGE_ORDINAL_FLAG) && pOrigin->u1.AddressOfData; pOrigin++) {

		PIMAGE_IMPORT_BY_NAME pName = reinterpret_cast<PIMAGE_IMPORT_BY_NAME>(pAddress + pOrigin->u1.AddressOfData);
		if (0 == ::_stricmp(_lp_sz_fun_name, reinterpret_cast<LPCSTR>(pName->Name))) {
			b_found = true;
			break;
		}
		pFirst ++;
	}

	if (b_found == false)
		return (m_error << TYPE_E_ELEMENTNOTFOUND);
	// https://docs.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-virtualprotect
	DWORD dwOrigin = NULL;
	if (FALSE == ::VirtualProtect((LPVOID) &(pFirst->u1.Function), sizeof(uintptr_t), PAGE_READWRITE, &dwOrigin))
		return ((CError&)m_error = ::GetLastError());

	pFirst->u1.Function = (uintptr_t)_p_new_fun;

	if (FALSE == ::VirtualProtect((LPVOID) &(pFirst->u1.Function), sizeof(uintptr_t), dwOrigin, &dwOrigin))
		return ((CError&)m_error = ::GetLastError());

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////