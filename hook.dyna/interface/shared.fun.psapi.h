#ifndef _SHAREDFUNPSAPI_H_994BF262_E445_4F2C_B71D_841BE5FCAE6E_INCLUDED
#define _SHAREDFUNPSAPI_H_994BF262_E445_4F2C_B71D_841BE5FCAE6E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 7-Jul-2020 at 10:42:44a, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Pack 'psapi.dll' inject functor interface declaration file.
*/
#include "shared.fun.base.h"
namespace ebo { namespace hide { namespace functors { namespace psapi {

	class CFunctor : public CFunctor_Base {
	                typedef CFunctor_Base TBase;

	public:
		 CFunctor (void);
		~CFunctor (void);

	public:
		HRESULT     Load   (void) override sealed;
		HRESULT     Replace(void) override sealed;
	};

}}}}

#endif/*_SHAREDFUNPSAPI_H_994BF262_E445_4F2C_B71D_841BE5FCAE6E_INCLUDED*/