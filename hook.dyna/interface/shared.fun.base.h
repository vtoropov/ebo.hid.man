#ifndef _SHAREDFUNBASE_H_52F5968F_7FFD_407A_AF07_4C5F03C8AFD0_INCLUDED
#define _SHAREDFUNBASE_H_52F5968F_7FFD_407A_AF07_4C5F03C8AFD0_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 7-Jul-2020 at 10:59:36a, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Pack functor usage base interface declaration file.
*/
#include "shared.hook.defs.h"
#include "shared.hook.reg.stg.h"

namespace ebo { namespace hide { namespace functors {

	using ebo::hide::CError_Ex;

	using ebo::hide::cfg::CList_Ex;
	using ebo::hide::cfg::CReg_Inject;

	class CFunctor_Base {
	protected:
		PIMAGE_IMPORT_DESCRIPTOR
		            m_import;
		MODULEINFO  m_info  ;  // psapi.h
		CError_Ex   m_error ;

	public:
		 CFunctor_Base (void);
		~CFunctor_Base (void);

	public:
		virtual HRESULT     Load   (void) PURE;
		virtual HRESULT     Replace(void) PURE;

	public:
		TErrorExRef Error  (void) const;
		const bool  Is     (void) const;

	public:
		HRESULT     Load   (LPCSTR _lp_sz_lib_name);   // loads dynamic library by name provided and initializes module info structure;
		HRESULT     Replace(LPCSTR _lp_sz_fun_name, PVOID const _p_new_fun);
	};

}}}
#endif/*_SHAREDFUNBASE_H_52F5968F_7FFD_407A_AF07_4C5F03C8AFD0_INCLUDED*/