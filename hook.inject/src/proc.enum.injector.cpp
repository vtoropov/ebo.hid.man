// proc.enum.console.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "StdAfx.h"
#include "shared.hook.reg.stg.h"

using ebo::hide::cfg::CList_Ex;
using ebo::hide::cfg::CReg_Inject;

// Define a type definition that is a function pointer
// HINSTANCE *fpLoadLibrary(char *);
// This will make creating our remote thread easier down the road.
typedef HINSTANCE(*fpLoadLibrary)(char *);


bool InjectDll(DWORD *processId) {

	LPVOID memAddress    = NULL;

	HINSTANCE hK32 = LoadLibraryA("KERNEL32");

	fpLoadLibrary fpLoadAddress = (fpLoadLibrary)GetProcAddress(hK32, "LoadLibraryA");

	HANDLE hProc = OpenProcess(PROCESS_ALL_ACCESS, NULL, *processId);

	if(!hProc) {
		return false;
	}

	printf("%s Captured HANDLE on %s:%d\n", "shared.hook_v15.dll", "proc.enum.console.exe", *processId);

	memAddress = VirtualAllocEx(hProc, NULL, _MAX_PATH, MEM_COMMIT, PAGE_EXECUTE_READWRITE);

	if(!memAddress) {
		return false;
	}

	bool memory = WriteProcessMemory(hProc, memAddress, "shared.hook_v15.dll", strlen("shared.hook_v15.dll") + 1, NULL);

	if(!memory) {
		return false;
	}

	HANDLE hProcThread = CreateRemoteThread(hProc, 0, 0, (LPTHREAD_START_ROUTINE)fpLoadAddress, memAddress, 0, 0);

	if(!hProcThread) {
		return false;
	}

	CloseHandle(hProcThread);
	CloseHandle(hProc);
	return memory;
}

INT    main(VOID) {

	// Process variables
	HANDLE hProc     = NULL;
	DWORD  processId = NULL;
	PROCESSENTRY32 pEntry = { sizeof(PROCESSENTRY32) };

	CReg_Inject reg_inj; reg_inj.Load();

	if (reg_inj.Targets().IsEmpty()) {
		::MessageBox(
			HWND_DESKTOP, _T("The registry does not contain a name of the app for injection."), _T("Error"), MB_ICONERROR | MB_OK
		);
		return 0;
	}

try_again__:
	if (hProc) {
		::CloseHandle(hProc); hProc = NULL;
	}
	bool b_found_app = false;

	hProc = ::CreateToolhelp32Snapshot(PROCESS_ALL_ACCESS, 0);
	if (NULL != hProc) {

		if (Process32First(hProc, &pEntry)) {
			do {
				CAtlStringW cs_app(pEntry.szExeFile); cs_app.MakeLower();

				const INT n_found_ndx = reg_inj.Targets().Find(cs_app.GetString());

				if (CList_Ex::e_not_found != n_found_ndx) {
					wprintf(
						_T("\n%s has been found!\nAttempting to inject %s\n"), reg_inj.Targets().ItemOf(n_found_ndx), _T("shared.hook_v15.dll")
					);
					processId = pEntry.th32ProcessID;

					if (processId) {
						InjectDll(&processId); b_found_app = true;
					}
				}
				// continues to find other process for case when several target apps are specified in registry list;
			} while(Process32Next(hProc, &pEntry));

			if (false == b_found_app) {
				if (IDOK == ::MessageBox(
					HWND_DESKTOP, _T("No application runs for the time being. Try again?"), _T("Warning"), MB_ICONEXCLAMATION | MB_OKCANCEL
				)) goto try_again__;
			}
		}
	}


	::CloseHandle(hProc); hProc = NULL;
	
	::_tprintf(_T("\n\n\tPress any key to exit"));
	::_gettch();

	return 0;
}