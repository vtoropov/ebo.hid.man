#pragma once
//
// ATL/OLE2-based header;
//
#include <atlbase.h>
#include <atlstr.h>             // important order, this file must be included before any includes of the WTL headers
#include <atlwin.h >
#include <atlcom.h >
#include <comdef.h >
using namespace ATL;

//
// Win API
//
#include <TlHelp32.h>
#include <Psapi.h>
#include <wtsapi32.h>

#pragma comment(lib, "__shared.lite_v15.lib")

#pragma comment(lib, "_ntfs_v15.lib" )
#pragma comment(lib, "_registry_v15.lib" )

#pragma comment(lib, "_user.32_v15.lib"  )

#pragma comment (lib, "shared.hook.lib_v15.lib")