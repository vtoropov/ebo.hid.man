#ifndef _SHAREDGENCMDLN_H_AF7E605A_D692_4F3E_A601_DD8DB84A6516_INCLUDED
#define _SHAREDGENCMDLN_H_AF7E605A_D692_4F3E_A601_DD8DB84A6516_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Apr-2012 at 07:46:04pm, GMT+3, Rostov-on-Don, Sunday;
	This is Pulsepay server application generic command line interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Platinum Clocking project on 19-Mar-2014 at 8:17:25am, GMT+4, Taganrog, Wednesday;
	Adopted to BotRevolt project on 21-Aug-2014 at 6:06:46am, GMT+4, Taganrog, Thursday;
	Adopted to FG (thefileguardian.com) project on 11-Jun-2016 at 1:28:28p, GMT+7, Phuket, Rawai, Saturday;
	Adopted to FakeGPS driver project on 13-Dec-2019 at 9:43:27a, UTC+7, Novosibirsk, Friday;
*/

namespace shared { namespace user32 {

	typedef ::std::map<::ATL::CAtlString, ::ATL::CAtlString> TCmdLineArgs;

	class CCommandLine {
	private:
		CAtlString   m_module_path;
		TCmdLineArgs m_args;
	public:
		 CCommandLine(void);
		~CCommandLine(void);
	public:
		HRESULT      Append(LPCTSTR _lp_sz_nm, LPCTSTR _lp_sz_val);
		CAtlString   Arg   (LPCTSTR _lp_sz_nm) const;
		LONG         Arg   (LPCTSTR _lp_sz_nm, const LONG _def_val)const;
		TCmdLineArgs Args  (void) const;                                  // returns a copy of command line argument collection
		VOID         Clear (void)      ;
		INT          Count (void) const;
		bool         Has   (LPCTSTR pszArgName)const;
		CAtlString   Path  (void) const;                                  // returns executable absolute path;
	public:
		CAtlString   ToString(LPCTSTR _lp_sz_sep = NULL) const;

	public:
		operator  LPCTSTR (void) const;               // returns command line object as a string;

	public:
		bool operator==(LPCTSTR pszArgName) const;    // finds an argument by name provided;
	};
}}

typedef shared::user32::CCommandLine TCommandLine;    // long alias;
typedef shared::user32::CCommandLine TCmdLine;        // short alias;

#endif/*_SHAREDGENCMDLN_H_AF7E605A_D692_4F3E_A601_DD8DB84A6516_INCLUDED*/