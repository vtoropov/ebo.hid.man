#ifndef _SHAREDGENVERINFO_H_E698FCA0_FB0B_4B9C_85FF_399CF39AA048_INCLUDED
#define _SHAREDGENVERINFO_H_E698FCA0_FB0B_4B9C_85FF_399CF39AA048_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Sep-2004 at 11:46:26a, UTC+6, Omsk, Thursday;
	This is very old code snippet of S-ModBus project for application version interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 27-May-2020 at 10:58:39a, UTC+7, Novosibirsk, Wednesday;
	Adopted to Ebo Xor on 28-May-2020 at 7:01:30p, UTC+7, Novosibirsk, Thursday;
*/
#include "shared.gen.sys.err.h"
namespace shared { namespace user32 {
	using shared::sys_core::CError;
	// https://docs.microsoft.com/en-us/windows/win32/menurc/versioninfo-resource
	// https://docs.microsoft.com/en-us/windows/win32/menurc/stringfileinfo-block
	class CVersion_Data {
	protected:
		LPVOID       m_pData;
		DWORD        m_dSize;

	public:
		 CVersion_Data (void);
		 CVersion_Data (const CVersion_Data&);
		~CVersion_Data (void);

	public:
		const bool   Is   (void) const;
		LPCVOID      Ptr  (void) const;
		LPVOID&      Ref  (void)      ;
		DWORD        Size (void) const;
		DWORD&       Size (void)      ;
	public:
		HRESULT      Clear(void);
	public:
		CVersion_Data& operator = (const CVersion_Data&);
	public:
		operator LPCVOID  (void) const;
		operator LPVOID   (void) const;
	};

	class CVer_Base {
	protected:
		mutable
		CError     m_error;

	protected:
		 CVer_Base (void);
		~CVer_Base (void);

	public:
		TErrorRef  Error (void) const;
	};

	class CVer_Product : public CVer_Base {
	                    typedef CVer_Base TBase;
	protected:
		CAtlString   m_name;     // Name of the product with which the file is distributed (required);
		CAtlString   m_version;  // Version of the product with which the file is distributed, for example, "3.10" or "5.00.RC2" (required);
		CAtlString   m_fixed;

	public:
		 CVer_Product (void);
		 CVer_Product (const CVer_Product&);
		~CVer_Product (void);

	public:
		LPCTSTR      Fixed  (void) const;
		HRESULT      Fixed  (LPCTSTR)   ;
		LPCTSTR      Name   (void) const;
		HRESULT      Name   (LPCTSTR)   ;
		LPCTSTR      Version(void) const;
		HRESULT      Version(LPCTSTR)   ;
	public:
		CAtlString   ToString(LPCTSTR _lp_sz_sep) const;
	public:
		CVer_Product& operator = (const CVer_Product&);
		CVer_Product& operator = (const CVersion_Data&);
	};

	class CVer_File : public CVer_Base {
	                 typedef CVer_Base TBase;
	public:
		enum _atts {
			e_desc   = 0x0,  // file description; File description to be presented to users; (required)
			e_vers   = 0x1,  // file version; Version number of the file, for example, "3.10" or "5.00.RC2"; (required)
			e_fixed  = 0x2,  // file version fixed;
			e_origin = 0x3,  // file original name; Original name of the file, not including a path; (required)
			e_intern = 0x4,  // file internal name; Internal name of the file, If no name, the original filename, without extension (required);
		};
	private:
		static const DWORD  dw_count = _atts::e_intern + 1;
	protected:
		CAtlString   m_atts[dw_count];

	public:
		 CVer_File (void);
		 CVer_File (const CVer_File&);
		~CVer_File (void);

	public:
		LPCTSTR     Att (const _atts)  const;
		HRESULT     Att (const _atts, LPCTSTR _lp_sz_val);
	public:
		LPCTSTR     Desc(void) const;
		HRESULT     Desc(LPCTSTR)   ;
	public:
		CAtlString  ToString(LPCTSTR _lp_sz_sep ) const;
	public:
		CVer_File&  operator = (const CVer_File&);
		CVer_File&  operator = (const CVersion_Data&);
	};

	class CVer_Vendor : public CVer_Base {
	                   typedef CVer_Base TBase;
	public:
		enum _atts {
			e_company   = 0x0, // Company that produced the file; (required)
			e_copyright = 0x1, // Copyright notices that apply to the file; (optional)
			e_trademark = 0x2, // Trademarks and registered trademarks that apply to the file; (optional)
			e_comments  = 0x3, // Additional information that should be displayed for diagnostic purposes; (optional)
		};
	private:
		static const DWORD  dw_count = _atts::e_comments + 1;
	protected:
		CAtlString   m_atts[dw_count];

	public:
		 CVer_Vendor (void);
		 CVer_Vendor (const CVer_Vendor&);
		~CVer_Vendor (void);

	public:
		LPCTSTR     Att (const _atts)  const;
		HRESULT     Att (const _atts, LPCTSTR _lp_sz_val);
	public:
		CAtlString  ToString(LPCTSTR _lp_sz_sep ) const;
	public:
		CVer_Vendor&  operator = (const CVer_Vendor&);
		CVer_Vendor&  operator = (const CVersion_Data&);
	};

	class CVersion : public CVer_Base {
	                typedef CVer_Base TBase;
	private:
		CVersion_Data m_pVerInfo;    // file version info data;
		CVer_File     m_file    ;    // file version attributes;
		CVer_Product  m_product ;    // product version attributes;
		CVer_Vendor   m_vendor  ;    // file producer/vendor data ;

	public:
		 CVersion(void);
		 CVersion(const CVersion&);
		 CVersion(LPCTSTR pszModulePath);
		~CVersion(void);

	public:
		HRESULT      Get (LPCTSTR pszModulePath);

	public:
		CAtlString   CustomValue   (LPCTSTR lpszValueName) const;

	public: // accessor(s)
		const CVer_File&    File   (void) const;
		const CVer_Product& Product(void) const;
		const CVer_Vendor&  Vendor (void) const;
		
	public:
		CVersion& operator = (const CVersion&);
	};

}}
typedef const shared::user32::CVer_File&     TVerFileRef;
typedef const shared::user32::CVer_Product&  TVerProdRef;
typedef const shared::user32::CVer_Vendor&   TVerVendRef;
typedef const shared::user32::CVersion&      TVersionRef;

#endif/*_SHAREDGENVERINFO_H_E698FCA0_FB0B_4B9C_85FF_399CF39AA048_INCLUDED*/