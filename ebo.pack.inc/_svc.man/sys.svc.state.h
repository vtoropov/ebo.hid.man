#ifndef _SYSSVCSTATE_H_0FA4E18F_97DF_4E5F_AC7F_87CA3B0EEAC0_INCLUDED
#define _SYSSVCSTATE_H_0FA4E18F_97DF_4E5F_AC7F_87CA3B0EEAC0_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Jul-2020 at 8:20:33a, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack system service control management service's state interface declaration file.
*/
#include "sys.svc.error.h"

namespace shared { namespace service {

	class CSvcState {
	public:
		enum _value : DWORD {
			eSvcStateUndefined   = 0x0,
			eSvcStopped          = SERVICE_STOPPED         ,
			eSvcStartPending     = SERVICE_START_PENDING   ,
			eSvcStopPanding      = SERVICE_STOP_PENDING    ,
			eSvcRunning          = SERVICE_RUNNING         ,
			eSvcContinuePending  = SERVICE_CONTINUE_PENDING,
			eSvcPausePending     = SERVICE_PAUSE_PENDING   ,
			eSvcPaused           = SERVICE_PAUSED  ,
			eSvcUninstalled      = eSvcPaused + 0x1,
			eSvcUninstallPending = eSvcPaused + 0x2,
		};
	private:
		_value        m_state;  // current state value;
		CAtlStringW   m_name ;
		CSvcError     m_error;
	public:
		 CSvcState(void);
		 CSvcState(const _value);
		 CSvcState(const _value , LPCWSTR pszName);
		 CSvcState(const CSvcState&);
		~CSvcState(void);

	public:
		_value        Current(void) const;
		VOID          Current(const _value);
		TErrorRef     Error       (void)const;
		CSvcError&    Error       (void);
		bool          IsRunning   (void)const;
		bool          IsStopped   (void)const;
		bool          IsTransitive(void)const;
		CAtlStringW   Name   (void) const;
		VOID          Name   (LPCWSTR)   ;

	public:
		CSvcState& operator = (const CSvcState&);
	};

	typedef CSvcState::_value        TSvcStateValue;
	typedef ::std::vector<CSvcState> TSvcStates;
	typedef const CSvcState&         TSvcStateRef;

	class CStateEnum
	{
	public:
		 CStateEnum (void);
		~CStateEnum (void);

	public:
		const INT     Count(void) const;
		TSvcStateRef  Item (const INT nIndex);
		const
		TSvcStates&   Raw  (void) const;
	};
}}

#endif/*_SYSSVCSTATE_H_0FA4E18F_97DF_4E5F_AC7F_87CA3B0EEAC0_INCLUDED*/