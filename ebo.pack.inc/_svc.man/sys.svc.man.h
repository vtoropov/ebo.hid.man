#ifndef _SHAREDSYSTEMSERVICEMAN_H_A9A4D670_9050_4562_9D73_9457363B8347_INCLUDED
#define _SHAREDSYSTEMSERVICEMAN_H_A9A4D670_9050_4562_9D73_9457363B8347_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Jun-2016 at 2:02:10p, UTC+7, Phuket, Rawai, Monday;
	This is Shared system service manager wrapper class declaration file. ( project: thefileguardian.com )
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 13-Jul-2020 at 9:46:02a, UTC+7, Novosibirsk, Monday;
*/
#include "sys.svc.error.h"
#include "sys.svc.create.h"
#include "sys.svc.object.h"

namespace shared { namespace service
{
	class CManagerParams
	{
	public:
		enum _e{
			eOpenForService = SC_MANAGER_ALL_ACCESS,
			eOpenForEnum    = SC_MANAGER_ENUMERATE_SERVICE
		};
	};

	class CManager
	{
	private:
		mutable
		CSvcError     m_error   ;
		SC_HANDLE     m_handle  ;
		TInfoList     m_services;
	public:
		 CManager(const SC_HANDLE = NULL);
		~CManager(void);
	public:
		HRESULT       Attach (const SC_HANDLE);
		HRESULT       Create (const CCrtData&, CService&);
		SC_HANDLE     Detach (void);
		HRESULT       EnumServices( const DWORD dwStatus);
		TErrorRef     Error  (void) const;
		HRESULT       Initialize  ( const DWORD dwAccess, const bool bIgnoreIfInited = false);
		bool          IsValid(void) const;
		HRESULT       Open   (const CCrtData&, const DWORD dwAccess, CService&);
		HRESULT       Open   (LPCWSTR lpszServiceName, const DWORD dwAccess, CService&);
		HRESULT       Remove (LPCWSTR lpszServiceName);
		const
		TInfoList&    Services(void)const;
		HRESULT       Start  (LPCWSTR lpszServiceName);
		HRESULT       Stop   (LPCWSTR lpszServiceName);
		HRESULT       Uninitialize(const bool bIgnoreIfUninited = false);

	public:
		operator SC_HANDLE(void) const;

	private: // copy protected;
		CManager(const CManager&);
		CManager&  operator= (const CManager&);

	public:
		CManager&  operator= (const SC_HANDLE);
	};
}}

#endif/*_SHAREDSYSTEMSERVICEMAN_H_A9A4D670_9050_4562_9D73_9457363B8347_INCLUDED*/