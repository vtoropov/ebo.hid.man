#ifndef _SYSSVCOBJECT_H_54EA6A52_EE05_4685_8E50_AB7EDCF70B94_INCLUDED
#define _SYSSVCOBJECT_H_54EA6A52_EE05_4685_8E50_AB7EDCF70B94_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Jan-2015 on 8:45:58p, UTC+3, Taganrog, Wednesday;
	This is shared system service manager library class(es) declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 12-Jul-2020 at 10:00:43a, UTC+7, Novosibirsk, Sunday;
*/
#include "sys.svc.error.h"
#include "sys.svc.state.h"
namespace shared { namespace service {
	class CInfo {
	public:
		enum _att_ndx : DWORD {
			e_name  = 0x0,
			e_desc  = 0x1,
			e_group = 0x2,
			e__max  = e_group + 1
		};
	private:
		CAtlStringW   m_atts[_att_ndx::e__max];
		DWORD         m_status;
	public:
		 CInfo (void);
		 CInfo (const CInfo&);
		 CInfo (const ENUM_SERVICE_STATUS&, const SC_HANDLE = NULL);
		~CInfo (void);
	public:
		LPCWSTR     Description(void)const;
		HRESULT     Description(LPCWSTR) ;
		bool        IsValid  (void) const;
		bool        IsRunning(void) const;
		LPCWSTR     Group    (void) const;
		HRESULT     Group    (LPCWSTR)   ;
		LPCWSTR     Name     (void) const;
		HRESULT     Name     (LPCWSTR)   ;
		DWORD       Status   (void) const;
		HRESULT     Status   (const DWORD);

	public:
		CInfo& operator = (const CInfo&);
	};

	typedef ::std::vector<CInfo> TInfoList;

	class CService
	{
	private:
		mutable
		CSvcError   m_error ;
		SC_HANDLE   m_handle;
		CSvcState   m_state ;

	public:
		 CService (SC_HANDLE = NULL);
		~CService (void);

	public:
		HRESULT     Attach (const SC_HANDLE);
		HRESULT     Close  (void);
		HRESULT     Description(LPCWSTR);  // sets a service description
		SC_HANDLE   Detach (void);
		TErrorRef   Error  (void)const;
		bool        IsValid(void)const;
		const
		CSvcState&  State  (void)const;
		CSvcState&  State  (void);
		HRESULT     UpdateCurrentState(void);
	public:
		CService&   operator=(SC_HANDLE);

	public:
		operator SC_HANDLE(void) const;

	private: // a service handle cannot be copied or duplicated; this class is copy protected;
		CService&   operator= (const CService&);
		CService (const CService& );
	};
}}

#endif/*_SYSSVCOBJECT_H_54EA6A52_EE05_4685_8E50_AB7EDCF70B94_INCLUDED*/