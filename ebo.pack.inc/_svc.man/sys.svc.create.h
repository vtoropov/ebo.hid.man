#ifndef _SYSSVCCREATE_H_F6FED3CC_D476_4d88_82AC_06C9BC8745C9_INCLUDED
#define _SYSSVCCREATE_H_F6FED3CC_D476_4d88_82AC_06C9BC8745C9_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Jun-2016 at 11:25:42a, UTC+7, Phuket, Rawai, Monday;
	This is Shared system service create data interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 11-Jul-2020 at 6:27:42p, UTC+7, Novosibirsk, Saturday;
*/
#include <AccCtrl.h>
#include <Aclapi.h>

#include "sys.svc.error.h"

namespace shared { namespace service {

	using shared::sys_core::CError;
	// https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/sc-create
	class CCrtOption { // service create option enumeration;
	public:
		enum _opt : DWORD {
			eNone           = 0x00,
			eStoppable      = 0x01,
			eCanShutdown    = 0x02,
			eCanContinue    = 0x04,
			eStartOnDemand  = 0x08,
			eFileSysDriver  = 0x10,
		};
	};

	typedef CCrtOption::_opt  TSvcCrtOpt;

	class CCrtData {
	public:
		enum _att_ndx {
			e_account  = 0x0,
			e_desc     = 0x1, // service description;
			e_name     = 0x2,
			e_path     = 0x3, // path to a service binary;
			e_title    = 0x4, // aka display name;
		};
	private:
		mutable
		CSvcError     m_error;
	private:
		CAtlStringW   m_atts[_att_ndx::e_title + 1];
		DWORD         m_dwOptions;
	public:
		 CCrtData(void);
		 CCrtData(const CCrtData&);
		~CCrtData(void);
	public:
		const
		CAtlStringW&   Account(void)    const;
		CAtlStringW&   Account(void)         ;
		const
		CAtlStringW&   Description(void)const;
		CAtlStringW&   Description(void)     ;
		const
		CAtlStringW&   DisplayName(void)const;
		CAtlStringW&   DisplayName(void)     ;
		TErrorRef      Error(void)      const;
		bool           IsFileSysDriver(void)const;
		bool           IsStartOnDemand(void)const;
		bool           IsValid(void)    const;
		const
		CAtlStringW&   Name(void)       const;
		CAtlStringW&   Name(void)            ;
		DWORD          Options(void)    const;
		DWORD&         Options(void)         ;
		const
		CAtlString&    Path(void)       const;
		CAtlString&    Path(void)            ;

	public:
		CAtlStringW    ToString(void)const;

	public:
		CCrtData& operator = (const CCrtData&);
	};

	class CServiceAccessLevel {
	public:
		enum _lvl : DWORD {
			eQueryStatus = SERVICE_QUERY_STATUS ,
		};
	};
}}

#endif/*_SYSSVCCREATE_H_F6FED3CC_D476_4d88_82AC_06C9BC8745C9_INCLUDED*/