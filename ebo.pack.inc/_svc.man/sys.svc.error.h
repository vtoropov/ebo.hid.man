#ifndef _SYSSVCERROR_H_2EA6DC14_6067_42B5_87AB_411AC69BF3D0_INCLUDED
#define _SYSSVCERROR_H_2EA6DC14_6067_42B5_87AB_411AC69BF3D0_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jul-2020 at 6:02:41p, UTC+7, Novosibirsk, Saturday;
	This is Ebo Pack system service control management error interface declaration file.
*/
#include "shared.gen.sys.err.h"
namespace shared { namespace service {

	using shared::sys_core::CError ;
	using shared::sys_core::CLang  ;
	using shared::sys_core::CErr_State;

	class CSvcError : public CError {
	                 typedef CError TBase;
	public:
		explicit CSvcError(LPCWSTR lpszModule = NULL);
		explicit CSvcError(const DWORD  dwError, const CLang& = CLang());
		explicit CSvcError(const HRESULT hError, const CLang& = CLang());
	public:
#pragma warning(disable:4481)
		virtual HRESULT Result(const HRESULT) override sealed;
#pragma warning(default:4481)
	public:
		CSvcError&  operator= (const DWORD  );
		CSvcError&  operator= (const HRESULT);
		CSvcError&  operator= (const CError&);
	};

}}

#endif/*_SYSSVCERROR_H_2EA6DC14_6067_42B5_87AB_411AC69BF3D0_INCLUDED*/