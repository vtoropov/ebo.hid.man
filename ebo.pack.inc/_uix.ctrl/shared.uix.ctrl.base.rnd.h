#ifndef _SHAREDUIXCTRLBASERND_H_52AD29E8_4263_4CE0_A167_79F1A5C3B3D9_INCLUDED
#define _SHAREDUIXCTRLBASERND_H_52AD29E8_4263_4CE0_A167_79F1A5C3B3D9_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Oct-2010 at 10:52:17pm, GMT+3, Rostov-on-Don, Sunday;
	This is Sfx Pack rendering base class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 29-Jul-2018 at 10:47:11a, UTC+7, Novosibirsk, Sunday;
	Adopted to FakeGPS project on 24-Apr-2020 at 10:46:59p, UTC+7, Novosibirsk, Friday;
*/
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.img.wrap.h"

namespace ex_ui { namespace controls {

	using ex_ui::draw::defs::IRenderer;
	using ex_ui::draw::CZBuffer;
	using ex_ui::draw::CImageCache;

	class CControlRenderer : public IRenderer {
	protected:
		CWindow             m_wnd;
		CControlCrt&        m_crt;
		CImageCache         m_images;

	public:
		 CControlRenderer(CControlCrt&);
		~CControlRenderer(void);

	public:
		virtual HRESULT     DrawBackground(const HDC hSurface, const RECT& rcDrawArea) override;
		virtual HRESULT     DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& rcDrawArea) const override;

	public:
#if (0)
		virtual HRESULT     DrawBkgnd(CZBuffer&);
#endif
		virtual HRESULT     DrawImage(CZBuffer&);
		virtual HRESULT     DrawImage(CZBuffer&, const RECT& rcDrawArea);
		virtual HRESULT     DrawImage(CZBuffer&, const RECT& rcDrawArea, const DWORD dwState);

	public:
		const
		CImageCache&   Images(void) const;
		CImageCache&   Images(void)      ;
		const
		CWindow&       Host  (void) const;
		CWindow&       Host  (void)      ;
		SIZE           RequiredSize(void) const;
	};
}}

#endif/*_SHAREDUIXCTRLBASERND_H_52AD29E8_4263_4CE0_A167_79F1A5C3B3D9_INCLUDED*/