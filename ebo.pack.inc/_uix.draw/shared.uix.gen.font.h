#ifndef __OWNERDRAW_GDIPLUS_WRAP_H_
#define __OWNERDRAW_GDIPLUS_WRAP_H_
/*
	I'm Vladimir Toropov (ebontrop@gmail.com) as a code writer transfer all ownership rights on this file
	with all exclusive intellectual rights for this product "FakeGPS" to Oleksii Gupa (aleksey.gupa@gmail.com).
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.gdi.draw.defs.h"

namespace ex_ui { namespace draw {

	using shared::sys_core::CError;

	class eCreateFontOption {
	public:
		enum _opt : ULONG {
			eNone          = 0x0000, // creates font with default (i.e. system height/bold/italic)
			eExactSize     = 0x0001, // creates font with specified size (irrelative)
			eRelativeSize  = 0x0002, // creates font with relative size (reduction/increasing)
			eBold          = 0x0004,
			eItalic        = 0x0008,
			eUnderline     = 0x0010,
		};
	};
}}

typedef ex_ui::draw::eCreateFontOption::_opt  TFontOpts;

namespace ex_ui { namespace draw {

	class CFont_Base {
	protected:
		HFONT     m_handle;   // font handle

	protected:
		 CFont_Base (void);
		~CFont_Base (void);

	public:
		HRESULT   Destroy(void);
		HFONT     Detach (void);
		HFONT     Handle (void) const;
		bool      Is     (void) const;
	public:
		operator
		const     HFONT  (void) const;
	private:
		CFont_Base(const CFont_Base&);
		CFont_Base& operator= (const CFont_Base&);
	};

	class CFont : public CFont_Base {
	             typedef CFont_Base TBase;
	private:
		bool      m_bManaged; // flag that indicates font handle must be destroyed (it the font is not stock one)
	public:
		 CFont(LPCTSTR pszFamily = NULL, const DWORD dwOptions = TFontOpts::eNone, const LONG lParam = 0);
		~CFont(void);
	public:
		HRESULT   Create (LPCTSTR pszFamily = NULL, const DWORD dwOptions = TFontOpts::eNone, const LONG lParam = 0);
		HFONT     Detach (void);
	};

	class CFontScalable : public CFont_Base {
	                     typedef CFont_Base TBase;
	public:
		 CFontScalable(const HDC, LPCTSTR lpszFontFamily, const INT nSize, const DWORD dwOptions = TFontOpts::eNone);
		~CFontScalable(void);

	public:
		HRESULT   Create (const HDC, LPCTSTR lpszFontFamily, const INT nSize, const DWORD dwOptions = TFontOpts::eNone);
	};

	class CLogFontInfo {
	protected:
		LOGFONT   m_lgf;
		CError    m_error;

	public:
		 CLogFontInfo (void);
		 CLogFontInfo (const LOGFONT&);
		~CLogFontInfo (void);

	public:
		CLogFontInfo& operator= (const LOGFONT& ref);

	public:
		HRESULT     Create(const DWORD dwOptions);
		TErrorRef   Error (void) const;
	public:
		operator const  LOGFONT&(void)
		const; operator LOGFONT&(void);
	};
}}

typedef ex_ui::draw::CFont  TFont;

#endif/*__OWNERDRAW_GDIPLUS_WRAP_H_*/