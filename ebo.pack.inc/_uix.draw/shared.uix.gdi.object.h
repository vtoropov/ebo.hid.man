#ifndef _UIXDRAWGDIOBJECT_H_758531BE_3252_454f_BDDE_151EB532BF99_INCLUDED
#define _UIXDRAWGDIOBJECT_H_758531BE_3252_454f_BDDE_151EB532BF99_INCLUDED
/*
	I'm Vladimir Toropov (ebontrop@gmail.com) as a code writer transfer all ownership rights on this file
	with all exclusive intellectual rights for this product "FakeGPS" to Oleksii Gupa (aleksey.gupa@gmail.com).
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.gdi.draw.defs.h"

namespace ex_ui { namespace draw {

	using shared::sys_core::CError;

	class CBitmapInfo : public BITMAP
	{
		typedef BITMAP    _inherited;
	private:
		HRESULT          m_hResult;         // result of getting bitmap info
		UINT             m_UID;             // identifier of this object
		HBITMAP          m_hBitmap;         // bitmap handle
	public:
		 CBitmapInfo(void);
		 CBitmapInfo(const HBITMAP hBitmap, const UINT UID = ::GetTickCount());
		~CBitmapInfo(void);
	public:
		HRESULT          AttachTo(const HBITMAP);
		HBITMAP          Detach(void);
		UINT             GetID(void) const;
		HRESULT          GetLastResult(void) const;
		HRESULT          GetSize(SIZE&) const;
	private:
		CBitmapInfo(const CBitmapInfo&);
		CBitmapInfo& operator=(const CBitmapInfo&);
	};

	class CDibSection {
	private:
		mutable
		HRESULT       m_hResult;    // last result
		HBITMAP       m_hBitmap;    // encapsulated bitmap descriptor
		PBYTE         m_pData;      // a pointer to bitmap bits
		SIZE          m_sz;         // bitmap size

	public:
		 CDibSection(void);
		 CDibSection(const HDC hDC, const SIZE& sz);
		~CDibSection(void);

	public:
		HRESULT           Create (const HDC hDC, const SIZE& sz);
		HBITMAP           Detach (void);                   // detaches from encapsulated bitmap descriptor
		const PBYTE       GetBits(void) const;             // gets bitmap data
		HBITMAP           GetHandle(void) const;           // gets a bitmap handle if success, otherwise returns zero
		HRESULT           GetLastResult(void) const;       // gets a last operation result
		bool              IsValid (void) const;            // checks a validity of the encapsulated bitmap descriptor
		Gdiplus::Bitmap*  ToBitmap(void) const;            // creates gdi+ bitmap object from encapsulated bitmap descriptor
	public:
		operator          HBITMAP() const;                 // operator overloading, does the same as GetHandle_Safe()
	};

	class CGdiplusBitmapWrap
	{
		Gdiplus::Bitmap*   m_pBitmap;
		bool               m_bManaged;
	public:
		 CGdiplusBitmapWrap(const HBITMAP); // creates GDI+ object from bitmap handle provided
		 CGdiplusBitmapWrap(Gdiplus::Bitmap*, const bool bManaged);
		~CGdiplusBitmapWrap(void);
	public:
		HRESULT            Clip(const RECT& rcDest, Gdiplus::Bitmap*& __out_ptr);
		Gdiplus::Bitmap*   Detach(void);
		HRESULT            Reset(void);
	private:
		CGdiplusBitmapWrap(const CGdiplusBitmapWrap&);
		CGdiplusBitmapWrap& operator= (const CGdiplusBitmapWrap&);
	};

	typedef  ::std::map<DWORD, Gdiplus::Bitmap*>  TImageCache;

	class CImageCache {
	private:
		TImageCache        m_cache;
	public:
		 CImageCache(void);
		~CImageCache(void);
	public:
		HRESULT            Add(const ULONG uKey, Gdiplus::Bitmap* pb);
		HRESULT            Add(const ULONG uKey, const SIZE& sz_, Gdiplus::Bitmap** ppb = NULL);
		HRESULT            Add(const DWORD uKey, const UINT nResId, const HINSTANCE hResourceModule = NULL);
		HRESULT            Clear(void);
		INT                Count(void) const;
		Gdiplus::Bitmap*   Get(const ULONG uKey) const;
		bool               Has(const ULONG uKey) const;
		HRESULT            Remove(const ULONG uKey);
		const TImageCache& Raw(void) const;

	public:
		operator const TImageCache& (void) const;

	private:
		CImageCache(const CImageCache&);
		CImageCache& operator=(const CImageCache&);
	};
}}

#endif/*_UIXDRAWGDIOBJECT_H_758531BE_3252_454f_BDDE_151EB532BF99_INCLUDED*/