#ifndef __SFXSOACLIENTSERVICEUIDRAWING_H_FF3BB206_8691_4a10_9FEC_65AC71A76E0D_INCLUDED
#define __SFXSOACLIENTSERVICEUIDRAWING_H_FF3BB206_8691_4a10_9FEC_65AC71A76E0D_INCLUDED
/*
	I'm Vladimir Toropov (ebontrop@gmail.com) as a code writer transfer all ownership rights on this file
	with all exclusive intellectual rights for this product "FakeGPS" to Oleksii Gupa (aleksey.gupa@gmail.com).
*/
#include "shared.uix.gdi.draw.defs.h"

namespace ex_ui { namespace draw {

	class CText_Base {
	protected:
		HDC      m_hdc;               // device context which this text works for;
		HFONT    m_fnt;               // font handle which this text is drawn by ;

	protected:
		 CText_Base (void);
		~CText_Base (void);

	public:
		bool     Is (const bool _b_re_check = false) const;

	public:
		CText_Base&   operator <<(const HFONT)  ;  // sets a font for making text size calculation;
		CText_Base&   operator <<(const HDC  )  ;  // sets a context device for getting text size ;

	public:
		CText_Base&   operator  =(const CText_Base&);

	public:
		static bool   Is (const HDC  );
		static bool   Is (const HFONT);
	};

	class CText : public CText_Base {
	             typedef CText_Base TBase;
		
	public:
		 CText(void);
		 CText(const HDC hDC, const HFONT hFont);
		~CText(void);
	public:
		SIZE     Size (LPCTSTR lpText) const; // gets a size of text provided;
		SIZE     Size (LPCTSTR lpText, RECT _rc_out) const;
	public:
		const
		SIZE     operator =  (LPCTSTR) const; // gets a size of text provided;
	};

	class CTextLine : public CText {
	                 typedef CText TText;
	private:
		SIZE       m_size; // dependable on font and device context;
		CAtlString m_text; // copy of original text;

	public:
		 CTextLine (void);
		~CTextLine (void);

	public:
		const
		SIZE&    Size (void) const;
		LPCTSTR  Text (void) const;
		HRESULT  Text (LPCTSTR)   ;
		HRESULT  Text (LPCTSTR, RECT _rc_out)   ;

	public:
		const
		SIZE&    operator =  (LPCTSTR);
	};

	typedef ::std::vector<CTextLine>   TTextLines;

	class CTextBlock : private CText_Base {
	                   typedef CText_Base TBase;
	protected:
		TTextLines    m_lines;
		RECT          m_rc_out;

	public:
		 CTextBlock (void);
		~CTextBlock (void);

	public:
		size_t        Count (void) const;
		const
		TTextLines&   Lines (void) const;

	public:
		const
		CTextLine&    Get   (const size_t _ndx) const;  // gets a line by index; if line is not exist, a reference to fake line is returned;
		CTextLine&    Get   (const size_t _ndx)      ;  // gets a line by index; if line is not exist, a reference to fake line is returned;
		const bool    Is    (void) const;

	public:
		const
		SIZE          operator = (LPCTSTR)      ;  // gets a size of a text provided;
		CTextBlock&   operator <<(const RECT&)  ;  // sets an outer/boundary rectangle;
	};
}}

#endif/*__SFXSOACLIENTSERVICEUIDRAWING_H_FF3BB206_8691_4a10_9FEC_65AC71A76E0D_INCLUDED*/