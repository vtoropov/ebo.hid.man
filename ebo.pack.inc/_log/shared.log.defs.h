#ifndef _SHAREDLOGCOMMONDEFS_H_54D16A79_8A77_47fe_AB9A_4C1B57F4F3E7_INCLUDED
#define _SHAREDLOGCOMMONDEFS_H_54D16A79_8A77_47fe_AB9A_4C1B57F4F3E7_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jul-2015 at 12:32:18pm, GMT+7, Phuket, Rawai, Saturday;
	This is Shared Log Library Common Definition declaration file.
*/

namespace shared { namespace log
{
	class eLogType {
	public:
		enum _e {
			eInfo    = 0,
			eWarning = 1,
			eError   = 2,
			eWaiting = 3,
		};
	};

	class eLogOption {
	public:
		enum _e{
			eNone       = 0x0,
			eUseJournal = 0x1, // system event journal;
			eUseFile    = 0x2  // file logger;
		};
	};

	interface ILogGenericCallback
	{
		virtual VOID ILog_OnGenericEvent(const eLogType::_e, CAtlString _evt_details) PURE;
	};
}}

#endif/*_SHAREDLOGCOMMONDEFS_H_54D16A79_8A77_47fe_AB9A_4C1B57F4F3E7_INCLUDED*/