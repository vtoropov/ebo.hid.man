// proc.enum.console.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "StdAfx.h"

// To ensure correct resolution of symbols, add Psapi.lib to TARGETLIBS
// and compile with -DPSAPI_VERSION=1

void PrintProcessNameAndID( DWORD processID )
{
	TCHAR szPath[MAX_PATH] = TEXT("<unknown>");

	HANDLE hProcess = ::OpenProcess( PROCESS_QUERY_INFORMATION |
		PROCESS_VM_READ,
		FALSE, processID );

	if (NULL == hProcess )
		return;
	
	GetModuleFileNameEx(hProcess, NULL, szPath, MAX_PATH - 1);
	_tprintf( TEXT("%s  (PID: %u)\n") , szPath, processID );

	HMODULE aModules[1024] = {0};
	DWORD cbNeeded = 0;

	if (FALSE == ::EnumProcessModules(hProcess, aModules, sizeof(aModules), &cbNeeded)) {

		cbNeeded /= sizeof(HMODULE);

		for (DWORD i_ = 0; i_ < cbNeeded; i_++) {
			::ZeroMemory(szPath, sizeof(szPath));
			if (GetModuleFileNameEx(hProcess, aModules[i_], szPath, MAX_PATH - 1)) {
				_tprintf( TEXT("Module: %s  (PID: %u)\n") , szPath, processID );
			}
		}
	}

	::CloseHandle( hProcess );
}

INT    Menu(VOID) {

	char value = 0;
	std::cout << std::endl
	<< "Please type menu item number and press [Enter]:" << std::endl
	<< "1) Get Process List;" << std::endl
	<< "2) Quit;" << std::endl
	<< "Action: ";
	std::cin >> value;
	switch (value) {
	case '1': return 1;
	}
	return 0;
}

INT    main(VOID) {

	// Get the list of process identifiers.

	DWORD aProcesses[1024] = {0}, cbNeeded = 0;

	INT n_action = Menu();

	while (1 == n_action) {

		if (FALSE == ::EnumProcesses( aProcesses, sizeof(aProcesses), &cbNeeded ) ) {
			break;
		}

		const DWORD dw_count = cbNeeded / sizeof(DWORD);

		// print the name and process identifier for each process.

		for (DWORD i_ = 0; i_ < dw_count && i_ < sizeof(aProcesses); i_++ ) {
			if( aProcesses[i_] != 0 ) {
				PrintProcessNameAndID( aProcesses[i_] );
			}
		}
		if (0 == dw_count) {
			::std::cout << "No Data;" << std::endl;
		}
		n_action = Menu();
	}

	::_tprintf(_T("\n\n\tPress any key to exit"));
	::_gettch();

	return 0;
}