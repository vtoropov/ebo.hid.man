#ifndef _STDAFX_H_9E3B03E0_F6FA_4255_B223_B08AF599075D_INCLUDED
#define _STDAFX_H_9E3B03E0_F6FA_4255_B223_B08AF599075D_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jul-2020 at 1:03:28p, UTC+7, Novosibirsk, Thursday;
	This is Ebo Pack process system hook library precompiled headers' declaration file.
*/
#ifndef STRICT
#define STRICT
#endif

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe
#pragma warning(disable: 4458)  // declaration of '{func_name}' hides class member (GDI+)

#define _ATL_APARTMENT_THREADED
#define _ATL_NO_AUTOMATIC_NAMESPACE
//
// ATL/OLE2-based header;
//
#include <atlbase.h>
#include <atlwin.h >
#include <atlcom.h >
#include <comdef.h >
#include <atlstr.h >
#include <atlsafe.h>

using namespace ATL;

#ifdef _DEBUG
	#define _ATL_DEBUG_INTERFACES
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
#endif
//
// Win API
//
#include <TlHelp32.h>
#include <Psapi.h>
#include <wtsapi32.h>
//
// STL-based headers;
//
#include <vector>
#include <map>
#include <time.h>
#include <typeinfo>

#pragma warning(disable: 6001) //uninitialized memory (bug with SAL notation)
#pragma warning(disable: 6031) //ignored return value warning
#pragma warning(disable: 6258) //TerminateThread warning

#endif/*_STDAFX_H_9E3B03E0_F6FA_4255_B223_B08AF599075D_INCLUDED*/