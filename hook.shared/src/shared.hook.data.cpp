/*
	Created by Tech_dog (ebontrop@gmail.com) on 7-Jul-2020 at 10:25:36a, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Pack process inject library data interface implementation file.
*/
#include "StdAfx.h"
#include "shared.hook.data.h"

using namespace ebo::hide::cfg;
/////////////////////////////////////////////////////////////////////////////

CList_Ex:: CList_Ex(void) {}
CList_Ex:: CList_Ex(const CList_Ex& _ref) : CList_Ex() { *this = _ref; }
CList_Ex::~CList_Ex(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CList_Ex::Append (LPCWCHAR _lp_sz_el) {
	if (NULL == _lp_sz_el || 0 == ::wcslen(_lp_sz_el))
		return E_INVALIDARG;
	try {
		CAtlStringW cs_el(_lp_sz_el); cs_el.MakeLower();
		m_elements.push_back(cs_el);
	}
	catch (const ::std::bad_alloc&) { return E_OUTOFMEMORY; }

	return S_OK;
}
HRESULT    CList_Ex::Clear  (VOID) {
	if (m_elements.empty() == false)
		m_elements.clear();
	return S_OK;
}
INT        CList_Ex::Find   (LPCWCHAR _lp_sz_el) const {
	if (this->IsEmpty())
		return _ndx::e_not_found;
	;
	if (NULL == _lp_sz_el || 0 == ::wcslen(_lp_sz_el))
		return _ndx::e_not_found;

	CAtlStringW cs_found(_lp_sz_el); cs_found.MakeLower();

	for (size_t i_ = 0; i_ < m_elements.size(); i_++) {

		if (-1 != cs_found.Find(m_elements[i_]))
			return static_cast<INT>(i_);
	}
	return _ndx::e_not_found;
}
bool       CList_Ex::IsEmpty(VOID) const  { return m_elements.empty(); }

LPCWCHAR   CList_Ex::ItemOf (const INT _ndx) const {
	if (0 > _ndx || _ndx >= static_cast<INT>(this->m_elements.size()))
		return NULL;
	else
		return this->m_elements[_ndx].GetString();
}

HRESULT    CList_Ex::Remove (LPCWCHAR _lp_sz_el) {
	if (this->IsEmpty())
		return OLE_E_BLANK;
	;
	if (NULL == _lp_sz_el || 0 == ::wcslen(_lp_sz_el))
		return E_INVALIDARG;

	CAtlStringW cs_found(_lp_sz_el); cs_found.MakeLower();

	for (size_t i_ = 0; i_ < m_elements.size(); i_++) {

		if (-1 != m_elements[i_].Find(cs_found)) {
			m_elements.erase(m_elements.begin() + i_);
			return S_OK;
		}
	}

	return TYPE_E_ELEMENTNOTFOUND;
}

HRESULT    CList_Ex::Split  (LPCWCHAR _lp_sz_lst, LPCWCHAR _lp_sz_sep) {
	_lp_sz_sep;
	if (NULL == _lp_sz_lst || 0 == ::_tcslen(_lp_sz_lst))
		return E_INVALIDARG;

	CAtlStringW cs_list(_lp_sz_lst);

	static LPCWSTR lp_sz_sep = L";";

	INT n_pos = 0;
	CAtlString cs_item = cs_list.Tokenize(lp_sz_sep, n_pos);

	while (cs_item.IsEmpty() == false) {
		try {
			m_elements.push_back(cs_item);
		}
		catch (::std::bad_alloc&) {
			return E_OUTOFMEMORY;
		}
		cs_item = cs_list.Tokenize(lp_sz_sep, n_pos);
	}
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CList_Ex& CList_Ex::operator += (LPCWCHAR _lp_sz_el) { this->Append(_lp_sz_el);  return *this; }
CList_Ex& CList_Ex::operator -= (LPCWCHAR _lp_sz_el) { this->Remove(_lp_sz_el);  return *this; }
CList_Ex& CList_Ex::operator  = (const CList_Ex& _ref) {

	this->Clear();
	for (size_t i_ = 0; i_ < _ref.m_elements.size(); i_++) {
		if (FAILED(this->Append(_ref.m_elements[i_])))
			break;
	}

	return *this;
}

/////////////////////////////////////////////////////////////////////////////

const bool CList_Ex::operator ==(LPCWCHAR _lp_sz_el) const { return (CList_Ex::e_not_found != this->Find(_lp_sz_el)); }