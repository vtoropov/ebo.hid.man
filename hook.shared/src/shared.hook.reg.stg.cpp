/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Jun-2020 at 6:53:30a, UTC+7, Novosibirsk, Saturday;
	This is Ebo Pack virtual audio cable driver connection data interface implementation file.
*/
#include "StdAfx.h"
#include "shared.hook.reg.stg.h"

using namespace ebo::hide::cfg;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace hide { namespace cfg { namespace _impl {

	class CReg_Folder {
	public:
		static LPCWSTR Root(void) { static LPCWSTR lp_sz_fld = L"Software\\Hide\\"; return lp_sz_fld; }
	};
	class CReg_Entries : public CReg_Folder {
	public:
		LPCWSTR Exclude (void) const {
			static CAtlStringW cs_in; if (cs_in.IsEmpty()) {
				cs_in += CReg_Folder::Root();
				cs_in += _T("Exclude");
			}
			return (LPCWSTR)cs_in;
		}
		LPCWSTR Target (void) const {
			static CAtlStringW cs_in; if (cs_in.IsEmpty()) {
				cs_in += CReg_Folder::Root();
				cs_in += _T("Target");
			}
			return (LPCWSTR)cs_in;
		}
		LPCWSTR Value (void) const {
			static LPCWSTR lp_sz_value = L"Module"; return lp_sz_value;
		}
	};

	class CReg_Error : public CReg_Folder {
	public:
		LPCWSTR Folder (void) const {
			static CAtlStringW cs_in; if (cs_in.IsEmpty()) {
				cs_in += CReg_Folder::Root();
				cs_in += _T("Error");
			}
			return (LPCWSTR)cs_in;
		}
		LPCWSTR Value_Code (void) const { static LPCWSTR lp_sz_value = L"Code"; return lp_sz_value; }
		LPCWSTR Value_Desc (void) const { static LPCWSTR lp_sz_value = L"Desc"; return lp_sz_value; }
	};

}}}}
using namespace ebo::hide::cfg::_impl;
/////////////////////////////////////////////////////////////////////////////

CRegistry_Base:: CRegistry_Base(void) : m_stg(HKEY_CURRENT_USER, CRegOptions::eDoNotModifyPath) { m_error << __FUNCTIONW__ << S_OK >> __FUNCTIONW__; }
CRegistry_Base::~CRegistry_Base(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef     CRegistry_Base::Error (void) const { return m_error; }
HRESULT       CRegistry_Base::Load  (CError& _out) {
	m_error << __FUNCTIONW__ << S_OK;

	HRESULT hr_ = S_OK;

	LONG l_loaded = S_OK;
	CAtlStringW  cs_desc;

	if (SUCCEEDED(hr_)) hr_ = this->m_stg.Load(CReg_Error().Folder(), CReg_Error().Value_Code(), l_loaded );
	if (SUCCEEDED(hr_)) hr_ = this->m_stg.Load(CReg_Error().Folder(), CReg_Error().Value_Desc(), cs_desc  );

	if (SUCCEEDED(hr_))
		_out.State().Set(static_cast<HRESULT>(l_loaded), cs_desc.GetString());
	else
		_out = (m_error = m_stg.Error());
	return m_error;
}

HRESULT       CRegistry_Base::Save  (TErrorRef _out) {
	m_error << __FUNCTIONW__ << S_OK;

	HRESULT hr_ = S_OK;

	CAtlStringW  cs_desc(_out.Desc());

	if (SUCCEEDED(hr_)) hr_ = this->m_stg.Save(CReg_Error().Folder(), CReg_Error().Value_Code(), static_cast<LONG>(_out.Result()));
	if (SUCCEEDED(hr_)) hr_ = this->m_stg.Save(CReg_Error().Folder(), CReg_Error().Value_Desc(), cs_desc  );

	if (FAILED(hr_))
		m_error = m_stg.Error();

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CReg_Common:: CReg_Common (void) : TBase() {}
CReg_Common::~CReg_Common (void) {}

/////////////////////////////////////////////////////////////////////////////

CReg_Inject:: CReg_Inject(void) : TBase() {}
CReg_Inject::~CReg_Inject(void) {}

/////////////////////////////////////////////////////////////////////////////
const
CList_Ex&   CReg_Inject::Excludes(void) const { return m_excludes; }
CList_Ex&   CReg_Inject::Excludes(void)       { return m_excludes; }
const
CList_Ex&   CReg_Inject::Targets (void) const { return m_targets ; }
CList_Ex&   CReg_Inject::Targets (void)       { return m_targets ; }

/////////////////////////////////////////////////////////////////////////////

HRESULT     CReg_Inject::Load   (void) {
	m_error << __FUNCTIONW__ << S_OK;
	HRESULT hr_ = S_OK;

	m_excludes.Clear();
	m_targets .Clear();

	CAtlStringW cs_procs;

	if (SUCCEEDED(hr_)) hr_ = TBase::m_stg.Load(CReg_Entries().Exclude(), CReg_Entries().Value(), cs_procs );
	if (SUCCEEDED(hr_)) {
		hr_ = m_excludes.Split(cs_procs.GetString());
	}

	CAtlStringW cs_targets;

	if (SUCCEEDED(hr_)) hr_ = TBase::m_stg.Load(CReg_Entries().Target (), CReg_Entries().Value(), cs_targets );
	if (SUCCEEDED(hr_)) {
		hr_ = m_targets.Split(cs_targets.GetString());
	}

	if (FAILED(hr_))
		m_error = TBase::m_stg.Error();

	return m_error;
}