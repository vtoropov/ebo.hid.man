/*
	Created by Tech_dog (ebontrop@gmail.com) on 7-Jul-2020 at 12:26:34p, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Pack shared hook common definition interface implementation file.
*/
#include "StdAfx.h"
#include "shared.hook.defs.h"
#include "shared.hook.reg.stg.h"

using namespace ebo::hide;
using namespace ebo::hide::cfg;

/////////////////////////////////////////////////////////////////////////////

CError_Ex:: CError_Ex(void) : TBase() { *this << __FUNCTIONW__ << S_OK >> __FUNCTIONW__; }
CError_Ex:: CError_Ex(const CError_Ex& _ref) : CError_Ex() { *this = _ref; }
CError_Ex::~CError_Ex(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CError_Ex::Load (void) {
	*this << __FUNCTIONW__ << S_OK;

	CReg_Common reg_stg;

	HRESULT hr_ = reg_stg.Load((TBase&)*this);
	if (FAILED(hr_))
		(TBase&)*this = reg_stg.Error();

	return *this;
}

HRESULT    CError_Ex::Save (void) {
	*this << __FUNCTIONW__ << S_OK;

	CReg_Common reg_stg;

	HRESULT hr_ = reg_stg.Save((TBase&)*this);
	if (FAILED(hr_))
		(TBase&)*this = reg_stg.Error();

	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CError_Ex& CError_Ex::operator = (const CError_Ex& _ref) {
	(TBase&)*this = (const TBase&)_ref;
	return  *this;
}
