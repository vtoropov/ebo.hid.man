#ifndef _SHAREDHOOKDATA_H_DEA28B36_5760_4F2D_93B4_55B41D6D389D_INCLUDED
#define _SHAREDHOOKDATA_H_DEA28B36_5760_4F2D_93B4_55B41D6D389D_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 7-Jul-2020 at 10:18:45a, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Pack process inject library data interface declaration file.
*/
#include "shared.gen.sys.err.h"
namespace ebo { namespace hide { namespace cfg {

	typedef ::std::vector<CAtlStringW> TModuleList;

	class CList_Ex{
	public:
		enum _ndx : INT {
			e_not_found = -1,
		};
	protected:
		TModuleList   m_elements;

	public:
		 CList_Ex (void);
		 CList_Ex (const CList_Ex&);
		~CList_Ex (void);

	public:
		HRESULT   Append (LPCWCHAR _lp_sz_el);
		HRESULT   Clear  (VOID) ;
		INT       Find   (LPCWCHAR _lp_sz_el) const; // it is assumed that registry value is not longer than real path that is provided;
		bool      IsEmpty(VOID) const;
		LPCWCHAR  ItemOf (const INT _ndx) const;
		HRESULT   Remove (LPCWCHAR _lp_sz_el);
		HRESULT   Split  (LPCWCHAR _lp_sz_lst, LPCWCHAR _lp_sz_sep = L";");

	public:
		CList_Ex& operator += (LPCWCHAR _lp_sz_el);
		CList_Ex& operator -= (LPCWCHAR _lp_sz_el);
		CList_Ex& operator  = (const CList_Ex&   );
	public:
		const bool operator ==(LPCWCHAR _lp_sz_el) const;
	};

}}}

#endif/*_SHAREDHOOKDATA_H_DEA28B36_5760_4F2D_93B4_55B41D6D389D_INCLUDED*/