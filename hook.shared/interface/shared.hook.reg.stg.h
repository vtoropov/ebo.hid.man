#ifndef _EBOVACREGSTG_H_F6E42514_1F61_4859_80A9_427284D37898_INCLUDED
#define _EBOVACREGSTG_H_F6E42514_1F61_4859_80A9_427284D37898_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Jun-2020 at 6:30:31a, UTC+7, Novosibirsk, Saturday;
	This is Ebo Pack virtual audio cable driver connection data interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.reg.hive.h"
#include "shared.hook.data.h"

namespace ebo { namespace hide { namespace cfg {

	using shared::sys_core::CError;
	using shared::registry::CRegistryStg;
	using shared::registry::CRegOptions ;

	class CRegistry_Base {
	protected:
		CRegistryStg   m_stg;
		CError         m_error;

	protected:
		 CRegistry_Base (void);
		~CRegistry_Base (void);

	public:
		TErrorRef     Error (void) const;
		HRESULT       Load  (CError& _out);    // restores error object from registry data;
		HRESULT       Save  (TErrorRef _out);  // saves outer error objects;

	};

	class CReg_Common : public CRegistry_Base {
	                   typedef CRegistry_Base TBase;
	public:
		 CReg_Common (void);
		~CReg_Common (void);
	};

	class CReg_Inject : public CRegistry_Base {
	                   typedef CRegistry_Base TBase;
	private:
		CList_Ex    m_excludes;
		CList_Ex    m_targets ;

	public:
		 CReg_Inject (void);
		~CReg_Inject (void);

	public:
		const
		CList_Ex&   Excludes(void) const;           // list of modules to exclude from the process list; (ro)
		CList_Ex&   Excludes(void)      ;           // list of modules to exclude from the process list; (rw)
		const
		CList_Ex&   Targets (void) const;           // list of applications which to inject DLL into process; (ro)
		CList_Ex&   Targets (void)      ;           // list of applications which to inject DLL into process; (rw)

	public:
		HRESULT     Load   (void);
	};
}}}
#endif/*_EBOVACREGSTG_H_F6E42514_1F61_4859_80A9_427284D37898_INCLUDED*/