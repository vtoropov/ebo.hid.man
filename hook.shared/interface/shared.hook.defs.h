#ifndef _SHAREDHOOKDEFS_H_CD3676D3_1848_40F9_9F1C_871E4A711FD3_INCLUDED
#define _SHAREDHOOKDEFS_H_CD3676D3_1848_40F9_9F1C_871E4A711FD3_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 7-Jul-2020 at 12:26:34p, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Pack shared hook common definition interface declaration file.
*/
#include "shared.gen.sys.err.h"
namespace ebo { namespace hide {

	using shared::sys_core::CError;

	class CError_Ex : public CError {
	                 typedef CError TBase;
	public:
		 CError_Ex (void);
		 CError_Ex (const CError_Ex&);
		~CError_Ex (void);

	public:
		HRESULT    Load (void); // loads this error from registry; for convenience only; registry storage implements this function;
		HRESULT    Save (void); // saves this error to registry; for convenience only; registry storage implements this function;

	public:
		CError_Ex& operator = (const CError_Ex&);
	};

}}

typedef const ebo::hide::CError_Ex&  TErrorExRef;

#endif/*_SHAREDHOOKDEFS_H_CD3676D3_1848_40F9_9F1C_871E4A711FD3_INCLUDED*/