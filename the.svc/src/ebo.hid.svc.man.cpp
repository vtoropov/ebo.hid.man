/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Jan-2016 at 1:18:50pm, GMT+7, Phuket, Rawai, Wednesday;
	This is File Guardian background service manager class implementation file.( project: https://thefileguardian.com )
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Hide Man project on 13-Jul-2020 at 1:04:26p, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"
#include "ebo.hid.svc.man.h"
#include "ebo.hid.svc.impl.h"
#include "ebo.hid.svc.crt.h"

using namespace ebo::hide::service;

#include "sys.svc.impl.h"
#include "sys.svc.man.h"

using namespace shared::service;

#include "ebo.hid.svc.res.h"

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace hide { namespace service { namespace _impl
{
	class CMan_CmdLineHandler
	{
	private:
		const
		TCommandLine& m_cmd_ln;
		CSvcError     m_error;

	public:
		CMan_CmdLineHandler(const TCommandLine& cmd_ln_ref) : m_cmd_ln(cmd_ln_ref) {
			m_error << __FUNCTIONW__ << S_OK >> __FUNCTIONW__;
		}
		~CMan_CmdLineHandler(void) {}

	public:
		TErrorRef Error      (void)const  { return m_error; }
		bool      IsCmdDebug (void)const  { return m_cmd_ln.Has(_T("debug"));   } // deprecated; global proprty can be used;
		bool      IsCmdCreate(void)const  { return m_cmd_ln.Has(_T("install")); }
		bool      IsCmdRemove(void)const  { return m_cmd_ln.Has(_T("remove"));  }
		bool      IsCmdStart (void)const  { return m_cmd_ln.Has(_T("start"));   }
		bool      IsCmdStop  (void)const  { return m_cmd_ln.Has(_T("stop"));    }

		HRESULT   Validate(void) {
			m_error << __FUNCTIONW__ << S_OK;

			if (this->IsCmdCreate() ||
				this->IsCmdRemove() ||
				this->IsCmdStart () ||
				this->IsCmdStop  () ||
				this->IsCmdDebug ())
				m_error.Clear();
			else
				m_error.State().Set(
						E_INVALIDARG, IDS_EBO_HID_SVC_CMDLINE_ERROR
					);
			return m_error;
		}
	};
}}}}
using namespace ebo::hide::service::_impl;
/////////////////////////////////////////////////////////////////////////////

THideSvcMan:: CManager(void) { m_error << __FUNCTIONW__ << S_OK >> __FUNCTIONW__; }
THideSvcMan::~CManager(void) { }

/////////////////////////////////////////////////////////////////////////////

TErrorRef  THideSvcMan::Error(void)const { return m_error; }

HRESULT    THideSvcMan::Process(const TCommandLine& cmd_ln_ref)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CMan_CmdLineHandler handler_(cmd_ln_ref);
	HRESULT hr_ = handler_.Validate();

	if (FAILED(hr_))
		return (m_error = handler_.Error());

	if (global::IsDebugMode())
	{
		CCrtData crt_data;
		hr_ = InitializeCrtData(crt_data);
		if (FAILED(hr_))
			return (m_error = hr_);

		CService svc_(crt_data);

		svc_.Start(0, NULL);
		::_getch();
		svc_.Stop();

		return m_error;
	}

	shared::service::CManager mgr_;
	CCrtData crt_;

	hr_ = InitializeCrtData(crt_);
	if (FAILED(hr_))
		return (m_error = hr_);

	hr_ = mgr_.Initialize(CManagerParams::eOpenForService);
	if (FAILED(hr_))
		return (m_error = mgr_.Error());

	if (false){}
	else if (handler_.IsCmdCreate())
	{
		CEventJournal::LogInfoFromResource(IDS_EBO_HID_SVC_CREATE_START);

		shared::service::CService svc_;
		hr_ = mgr_.Create(crt_, svc_);

		if (FAILED(hr_))
			m_error = mgr_.Error();
		else
			CEventJournal::LogInfoFromResource(IDS_EBO_HID_SVC_CREATE_SUCCESS);
	}
	else if (handler_.IsCmdRemove())
	{
		CEventJournal::LogInfoFromResource(IDS_EBO_HID_SVC_REMOVE_START);

		hr_ = mgr_.Remove(crt_.Name());
		if (FAILED(hr_))
			m_error = mgr_.Error();
		else
			CEventJournal::LogInfoFromResource(IDS_EBO_HID_SVC_REMOVE_SUCCESS);
	}
	else if (handler_.IsCmdStart())
	{
		CEventJournal::LogInfoFromResource(IDS_EBO_HID_SVC_RUN_START);

		hr_ = mgr_.Start(crt_.Name());
		if (FAILED(hr_))
			m_error = mgr_.Error();
		else
			CEventJournal::LogInfoFromResource(IDS_EBO_HID_SVC_RUN_SUCCESS);
	}
	else if (handler_.IsCmdStop())
	{
		CEventJournal::LogInfoFromResource(IDS_EBO_HID_SVC_STOP_STARTED);

		hr_ = mgr_.Stop(crt_.Name());
		if (FAILED(hr_))
			m_error = mgr_.Error();
		else
			CEventJournal::LogInfoFromResource(IDS_EBO_HID_SVC_STOP_SUCCESS);
	}
	return m_error;
}