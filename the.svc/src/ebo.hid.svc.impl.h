#ifndef _FGSERVICEIMPL_H_81C2D44B_F26F_433f_B79B_90DCEF0283B2_INCLUDED
#define _FGSERVICEIMPL_H_81C2D44B_F26F_433f_B79B_90DCEF0283B2_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 2:40:18am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian background service main class declaration file.
	( project: https://thefileguardian.com )
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Hide Man project on 13-Jul-2020 at 11:12:11a, UTC+7, Novosibirsk, Monday;
*/
#include "shared.gen.sys.err.h"
#include "shared.gen.syn.obj.h"

#include "sys.svc.impl.h"
#include "sys.svc.error.h"
#include "ebo.hid.svc.defs.h"

namespace ebo { namespace hide { namespace service {

	using shared::sys_core::CError;
	using shared::sys_core::CSyncObject;

	using shared::service::CCrtData ;
	using shared::service::CSvcError;
	using shared::service::CServiceBaseImpl;
	using shared::service::CServiceBaseImplEx;

	class CService : public CServiceBaseImplEx {
	                typedef CServiceBaseImplEx TBase;
	private:
		CThreadCrtData  m_crt;
		CSyncObject     m_sync_obj;
		CSvcError       m_error;

	public:
		 CService(const CCrtData&);
		~CService(void);
#pragma warning(disable: 4481)
	private: // CServiceBaseImpl
		VOID    OnStart(const DWORD dwArgc, PWSTR* pszArgv) override sealed;
		VOID    OnStop(VOID) override sealed;

	private: // CServiceBaseImplEx
		VOID    OnDeviceEvent(const DWORD dwEventType, LPVOID const lpEventData) override sealed;
#pragma warning(default: 4481)
	public:
		CError  Error (void) const;

	private:
		VOID    WorkerThread(void);
	};
}}}

#endif/*_FGSERVICEIMPL_H_81C2D44B_F26F_433f_B79B_90DCEF0283B2_INCLUDED*/