/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 3:25:44am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian background service main class implementation file. ( project: https://thefileguardian.com )
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Hide Man project on 13-Jul-2020 at 11:24:42a, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"
#include "ebo.hid.svc.impl.h"

using namespace ebo::hide::service;

#include "shared.gen.sys.com.h"
#include "shared.gen.app.obj.h"
#include "shared.run.event.h"
#include "shared.runner.tpl.h"

using namespace shared::sys_core;
using namespace shared::user32;
using namespace shared::runnable;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace hide { namespace service { namespace _impl {

}}}}
using namespace ebo::hide::service::_impl;
/////////////////////////////////////////////////////////////////////////////

CService:: CService (const CCrtData& crt_data) : TBase(crt_data) { m_error << __FUNCTIONW__ << S_OK >> __FUNCTIONW__; }
CService::~CService (void) { }

/////////////////////////////////////////////////////////////////////////////

VOID   CService::OnStart(const DWORD dwArgc, PWSTR* pszArgv) {
	m_error << __FUNCTIONW__ << S_OK;

	TBase::OnStart(dwArgc, pszArgv);

	if (m_last_error != NO_ERROR) {}

	if (!CThreadPool::QueueUserWorkItem(&CService::WorkerThread, this))
		m_error = ::GetLastError();
	else
	{
		// the code below is temporarily disabled due to the error:
		// code= 0x80070534; desc=No mapping between account names and security IDs was done;
	}
}

VOID   CService::OnStop (VOID) {
	m_error << __FUNCTIONW__ << S_OK;

	m_crt.IsStopped(true);

	TBase::OnStop();

	if (::WaitForSingleObject(m_crt.EventObject(), INFINITE) != WAIT_OBJECT_0)
		m_error = ::GetLastError();
}

/////////////////////////////////////////////////////////////////////////////

VOID   CService::OnDeviceEvent(const DWORD dwEventType, LPVOID const lpEventData) {
	dwEventType; lpEventData;
}

/////////////////////////////////////////////////////////////////////////////

CError CService::Error(void) const {
	CError err_obj; {
		SAFE_LOCK(m_sync_obj);
		err_obj = m_error;
	}
	return err_obj;
}

/////////////////////////////////////////////////////////////////////////////

VOID   CService::WorkerThread(void) {
	m_error << __FUNCTIONW__ << S_OK;
	CCoIniter com_core(false);

	CDelayEvent waiter_(50, 500);

	while (!m_crt.IsStopped())
	{
		waiter_.Wait();
		if (waiter_.Elapsed())
			waiter_.Reset();
		else
			continue;
	}
	::SetEvent(m_crt.EventObject());
}