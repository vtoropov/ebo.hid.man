#ifndef _FGSERVICEMGR_H_9A94A259_C36B_43d2_B806_C62B29F0DF20_INCLUDED
#define _FGSERVICEMGR_H_9A94A259_C36B_43d2_B806_C62B29F0DF20_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Jan-2016 at 1:10:50pm, GMT+7, Phuket, Rawai, Wednesday;
	This is File Guardian background service manager class declaration file.( project: https://thefileguardian.com )
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Hide Man project on 13-Jul-2020 at 12:52:00p, UTC+7, Novosibirsk, Monday;
*/
#include "shared.app.cmd.ln.h"
#include "sys.svc.error.h"

namespace ebo { namespace hide { namespace service
{
	using shared::service::CSvcError;

	class CManager
	{
	private:
		CSvcError   m_error;

	public:
		 CManager(void);
		~CManager(void);

	public:
		TErrorRef  Error  (void) const;
		HRESULT    Process(const TCommandLine&);
	};
}}}

typedef ebo::hide::service::CManager  THideSvcMan;

#endif/*_FGSERVICEMGR_H_9A94A259_C36B_43d2_B806_C62B29F0DF20_INCLUDED*/