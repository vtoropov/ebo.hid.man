/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 2:05:26am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian Background Service Application precompiled header implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Hide Man project on 13-Jul-2020 at 6:22:29a, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"

using namespace shared::user32;

namespace global
{
	bool    IsDebugMode(void)
	{
		static volatile bool bDebugMode  = false;
		static volatile bool bInitialized = false;
		if (bInitialized == false)
		{
			bInitialized  = true;
			const TCommandLine& cmd_line = GetAppObjectRef().CommandLine();
			bDebugMode = cmd_line.Has(_T("debug"));
		}
		return bDebugMode;
	}
}