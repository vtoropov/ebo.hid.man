#ifndef _FGSERVICEPRECOMPILEDHEADER_H_BC66510F_407B_4447_88A9_5C36BEF3454C_INCLUDED
#define _FGSERVICEPRECOMPILEDHEADER_H_BC66510F_407B_4447_88A9_5C36BEF3454C_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 2:02:59am, GMT+7, Phuket, Rawai, friday;
	This is File Guardian Background Service Application precompiled header declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Hide Man project on 13-Jul-2020 at 6:20:53a, UTC+7, Novosibirsk, Monday;
*/

#define  WINVER         0x0600
#define _WIN32_WINNT    0x0601
#define _WIN32_IE       0x0700
#define _RICHEDIT_VER   0x0200
//
// ATL
//
#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <conio.h>
#include <comutil.h>
//
// STL
//
#include <vector>
#include <map>
#include <typeinfo>

#pragma warning(disable: 4481) // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996) // security warning: function or variable may be unsafe
//
// EBO
//
#include "shared.gen.app.obj.h"
#include "shared.log.journal.h"

using shared::log::CEventJournal;

namespace global
{
	bool   IsDebugMode(void);
	class _out : public shared::log::CEventJournal {};
}

#pragma warning(disable: 4099) // mcrypt.lib: linking object as if no debug info

#pragma comment(lib, "__shared.lite_v15.lib")
#pragma comment(lib, "_log_v15.lib")
#pragma comment(lib, "_ntfs_v15.lib")
#pragma comment(lib, "_runnable_v15.lib")
#pragma comment(lib, "_svc.man_v15.lib")
#pragma comment(lib, "_user.32_v15.lib")

#pragma comment(lib, "ebo.hid.shared.lib")

#endif/*_FGSERVICEPRECOMPILEDHEADER_H_BC66510F_407B_4447_88A9_5C36BEF3454C_INCLUDED*/