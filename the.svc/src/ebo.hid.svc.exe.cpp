/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 2:30:37am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian background service entry point implementation file.( project: https://thefileguardian.com )
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Hide Man project on 13-Jul-2020 at 12:31:52p, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"
#include "shared.gen.sys.com.h"
#include "shared.app.console.h"
#include "shared.gen.sys.date.h"

using namespace shared::user32;
using namespace shared::sys_core;
using namespace shared::common;

#include "ebo.hid.svc.defs.h"
#include "ebo.hid.svc.impl.h"
#include "ebo.hid.svc.crt.h"
#include "ebo.hid.svc.man.h"

using namespace ebo::hide::service;

#include "ebo.hid.svc.res.h"

INT _tmain(VOID)
{
	CCoApartmentThreaded com_core;
	if (!com_core.IsSuccess()) {
		return 1;
	}

	const bool debug_ = global::IsDebugMode();
	const TCommandLine& cmd_line = GetAppObjectRef().CommandLine();

	if (debug_)
	{
		global::_out::OutputToConsole(debug_);
		global::_out::VerboseMode(debug_);
		global::_out::LogInfoFromResource(
				IDS_EBO_HID_SVC_RUN_STANDALONE
			);
		global::_out::LogInfoFromResource(
				IDS_EBO_HID_SVC_CLS_WINDOW
			);
	}

	CError err_; err_ << __FUNCTIONW__ << S_OK >> __FUNCTIONW__;

	if (0 == cmd_line.Count()) // it very looks like the module is loaded by system service manager
	{
		HRESULT hr_ = S_OK;
		CCrtData crt_data;
		hr_ = InitializeCrtData(crt_data);
		if (SUCCEEDED(hr_))
		{
			CService svc_(crt_data);
			// https://docs.microsoft.com/en-us/windows/win32/api/winsvc/nf-winsvc-startservicectrldispatchera
			const BOOL bResult = CService::Run(svc_);
			if (FALSE == bResult)
			{
				err_.Last();
				if (ERROR_FAILED_SERVICE_CONTROLLER_CONNECT == err_.Code()) {
					err_.State().Set( err_.Code(), (LPCWSTR)_T("This error is returned if the program is being run as a console application rather than as a service."));
					err_.Show ();
				}
				else {
				global::_out::LogError(
						_T("Service failed to run: %s"), CErr_Format(err_).Do().GetString()
					);
				}
			}
		}
	}
	else {
		CEventJournal::OutputToConsole(true);

		HRESULT hr_ = S_OK;
		
		CConsoleWindow console_;
		console_.SetIcon(IDR_RT_MAINICON);
		::SetConsoleOutputCP(CP_UTF8);
		//
		// filter will be connected by service worker thread; 
		//
		if (SUCCEEDED(hr_))
		{
			CManager mgr_;

			hr_ = mgr_.Process(cmd_line);
			if (FAILED(hr_))
				CEventJournal::LogError(
						mgr_.Error()
					);
		}
	}

	if (debug_) {
		CEventJournal::LogEmptyLine();
		CEventJournal::LogInfo(
				_T("Ebo Hide Man service is exiting. Press any key or click [x] button")
			);
		::_gettch();
	}

	return 0;
}