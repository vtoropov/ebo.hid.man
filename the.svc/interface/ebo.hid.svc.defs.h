#ifndef _FGSERVICECOMMONDEFS_H_F6E3B79B_9A0A_43de_BB20_D459205D8C9A_INCLUDED
#define _FGSERVICECOMMONDEFS_H_F6E3B79B_9A0A_43de_BB20_D459205D8C9A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 2:34:36am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian background service common definitions declaration file.( project: https://thefileguardian.com )
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Hide Man project on 13-Jul-2020 at 8:54:30a, UTC+7, Novosibirsk, Monday;
*/
#include "sys.svc.create.h"
#include "shared.runner.tpl.h"

namespace ebo { namespace hide {  namespace service
{
	using shared::service::CCrtData  ;
	using shared::service::TSvcCrtOpt;
	using shared::service::CSvcError ;

	using shared::sys_core::CThreadCrtData;
}}}

#endif/*_FGSERVICECOMMONDEFS_H_F6E3B79B_9A0A_43de_BB20_D459205D8C9A_INCLUDED*/